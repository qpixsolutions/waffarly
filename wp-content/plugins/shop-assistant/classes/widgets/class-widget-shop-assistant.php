<?php
/**
 * 
 * Shop Assistant Widget
 *
 * @package   Shop-Assistant
 * @author    cxThemes
 * @category  WooCommerce
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WC_Widget_Shop_Assistant extends WP_Widget {

	var $woo_widget_cssclass;
	var $woo_widget_description;
	var $woo_widget_idbase;
	var $woo_widget_name;

	/**
	 * constructor
	 *
	 * @access public
	 * @return void
	 */
	function WC_Widget_Shop_Assistant() {
		
		add_action( 'load-widgets.php', array(&$this, 'load_colorpicker') );

		/* Widget variable settings. */
		$this->woo_widget_cssclass 		= 'woocommerce widget_shop_assistant';
		$this->woo_widget_description	= __( 'Shows a sentence where the customer can configure a specific search.', 'shop-assistant' );
		$this->woo_widget_idbase 		= 'woocommerce_shop_assistant';
		$this->woo_widget_name 			= __( 'WooCommerce Shop Assistant', 'shop-assistant' );

		/* Widget settings. */
		$widget_ops = array( 'classname' => $this->woo_widget_cssclass, 'description' => $this->woo_widget_description );

		/* Create the widget. */
		$this->WP_Widget( 'woocommerce_shop_assistant', $this->woo_widget_name, $widget_ops );
	}
	
	function load_colorpicker() {
		global $plugin_dir_path_shop_assistant;
		
		wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'wp-color-picker' );
		
		wp_register_style( 'shop-assistant-widget', plugins_url( $plugin_dir_path_shop_assistant . '/css/widget-style.css', basename( __FILE__ ) ), '', '1.04', 'screen' );
		wp_enqueue_style( 'shop-assistant-widget' );
		wp_register_script( 'shop-assistant-widget', plugins_url( $plugin_dir_path_shop_assistant . '/js/shop-assistant-widget.js', basename( __FILE__ ) ), array("jquery"), '1.04');
		wp_enqueue_script( 'shop-assistant-widget' );
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {

		extract( $args );
		
		$title 	= apply_filters('widget_title', $instance['shop_assistant_title'], $instance, $this->id_base);
		$color 	= isset( $instance['shop_assistant_color'] ) ? $instance['shop_assistant_color'] : '#8e9396';
		$theme 	= isset( $instance['shop_assistant_theme'] ) ? $instance['shop_assistant_theme'] : 'none';
		$font_size 	= isset( $instance['shop_assistant_font_size'] ) ? $instance['shop_assistant_font_size'] : '26';

		echo do_shortcode("[shop_assistant title='".$title."' color='".$color."' theme='".$theme."' font_size='".$font_size."' ]");
	}

	/**
	 * update function.
	 *
	 * @see WP_Widget->update
	 * @access public
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		global $woocommerce;

		$instance['shop_assistant_title'] = strip_tags( stripslashes($new_instance['shop_assistant_title'] ) );
		$instance['shop_assistant_color'] = stripslashes( $new_instance['shop_assistant_color'] );
		$instance['shop_assistant_theme'] = stripslashes( $new_instance['shop_assistant_theme'] );
		$instance['shop_assistant_font_size'] = stripslashes( $new_instance['shop_assistant_font_size'] );

		return $instance;
	}

	/**
	 * form function.
	 *
	 * @see WP_Widget->form
	 * @access public
	 * @param array $instance
	 * @return void
	 */
	function form( $instance ) {
		global $woocommerce;

		if ( ! isset( $instance['shop_assistant_title'] ) )
			$instance['shop_assistant_title'] = '';

		if ( ! isset( $instance['shop_assistant_color'] ) )
			$instance['shop_assistant_color'] = '#8e9396';

		if ( ! isset( $instance['shop_assistant_theme'] ) )
			$instance['shop_assistant_theme'] = 'light';

		if ( ! isset( $instance['shop_assistant_font_size'] ) )
			$instance['shop_assistant_font_size'] = '26';

		?>

		<div class="shop-assistant-widget">
		
			<p>
				<label><?php _e("Search Phrase:", "shop-assistant"); ?></label>
				
				<?php $current_phrase = $this->get_outputted_text(); ?>
				
				<?php if($current_phrase): ?>				
					<div class="widget-phrase">
						<?php echo $this->get_outputted_text(); ?>
					</div>
				<?php else: ?>
					<div class="widget-no-phrase">
						<?php _e("Please set a search phrase at WooCommerce > Shop Assitant", "shop-assistant"); ?>
					</div>
				<?php endif; ?>
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'shop_assistant_title' ); ?>"><?php _e( 'Title:', 'shop-assistant' ) ?></label>
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'shop_assistant_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'shop_assistant_title' ) ); ?>" value="<?php if ( isset( $instance['shop_assistant_title'] ) ) echo esc_attr( $instance['shop_assistant_title'] ); ?>" />
				<small><?php _e("This is optional and appears above the widget.", "shop-assistant"); ?></small>
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'shop_assistant_theme' ); ?>"><?php _e( 'Theme:', 'shop-assistant' ) ?></label>
				<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'shop_assistant_theme' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'shop_assistant_theme' ) ); ?>" >
					<option value="none" <?php if ( isset( $instance['shop_assistant_theme'] ) && $instance['shop_assistant_theme'] == "none" ) echo "selected='selected'"; ?>><?php _e("None", "shop-assistant"); ?></option>
					<option value="light" <?php if ( isset( $instance['shop_assistant_theme'] ) && $instance['shop_assistant_theme'] == "light" ) echo "selected='selected'"; ?>><?php _e("Light", "shop-assistant"); ?></option>
					<option value="dark" <?php if ( isset( $instance['shop_assistant_theme'] ) && $instance['shop_assistant_theme'] == "dark" ) echo "selected='selected'"; ?>><?php _e("Dark", "shop-assistant"); ?></option>
					<option value="clean" <?php if ( isset( $instance['shop_assistant_theme'] ) && $instance['shop_assistant_theme'] == "clean" ) echo "selected='selected'"; ?>><?php _e("Clean", "shop-assistant"); ?></option>
				</select>
				<small><?php _e("The overall style of this widget.", "shop-assistant"); ?></small>
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'shop_assistant_color' ); ?>"><?php _e( 'Color:', 'shop-assistant' ) ?></label>
				<input type="text" class="shop-assistant-color" id="<?php echo esc_attr( $this->get_field_id( 'shop_assistant_color' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'shop_assistant_color' ) ); ?>" value="<?php if ( isset( $instance['shop_assistant_color'] ) ) echo esc_attr( $instance['shop_assistant_color'] ); ?>" data-default-color="<?php if ( isset( $instance['shop_assistant_color'] ) ) echo esc_attr( $instance['shop_assistant_color'] ); ?>" />
				<br/>
				<small><?php _e("The color of the clickable text in the widget.", "shop-assistant"); ?></small>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'shop_assistant_font_size' ); ?>"><?php _e( 'Font Size:', 'shop-assistant' ) ?></label>
				<input type="text" class="small-number" id="<?php echo esc_attr( $this->get_field_id( 'shop_assistant_font_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'shop_assistant_font_size' ) ); ?>" value="<?php if ( isset( $instance['shop_assistant_font_size'] ) ) echo esc_attr( $instance['shop_assistant_font_size'] ); ?>" /> px
				<small><?php _e("The search phrase font size.", "shop-assistant"); ?></small>
			</p>
			
			<script type='text/javascript'>
			
				var elems = jQuery('#widgets-right .shop-assistant-color, .inactive-sidebar .shop-assistant-color');
			
				var color_options = {
				    // hide the color picker controls on load
				    hide: true,
				    // show a group of common colors beneath the square
				    // or, supply an array of colors to customize further
				    palettes: true
				};
				
				jQuery(document).ready(function($) {
					
	            	elems.wpColorPicker(color_options);
	            	
		        }).ajaxComplete(function(e, xhr, settings) {
		        	
		            if( settings.data.search('action=save-widget') != -1 && settings.data.search( 'id_base=shop_assistant_color' ) != -1 ) {  
		                elems.wpColorPicker(color_options);
		            }
		            
		        });
		        
	        </script>

	    </div>

		<?php
	}
	
	function get_outputted_text() {
		$output = "";
		
		$form_fields = get_option('shop-assistant-fields');
		
		if (empty($form_fields)) {
			return;
		} else {
			$form_fields = unserialize($form_fields);
		}
		
		foreach ($form_fields as $form_field) {
			
			$text = $form_field["text"];
			$filter = $form_field["filter"];
			$priceone = $form_field["priceone"];
			$pricetwo = $form_field["pricetwo"];
			$label = $form_field["label"];
			
			if ( $filter != "" ){
				
				$output .= $text." ";
				
				if ( $filter == "price" ) {
					$output .= $priceone." & ".$pricetwo." ";
				}
				else {
					$output .= $label." ";
				}
			}
			
		}
		return $output;
	}
}
