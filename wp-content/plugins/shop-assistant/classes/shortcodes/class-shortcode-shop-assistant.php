<?php
/**
 * Shop Assistant Shortcode
 *
 * Shows the shop assistant
 *
 * @author 		WooThemes
 * @category 	Shortcodes
 * @package 	Shop-Assistant/Shortcodes/Shop_Assistant
 * @version     2.0.0
 */

class WC_Shortcode_Shop_Assistant {

	/**
	 * Get the shortcode content.
	 *
	 * @access public
	 * @param array $atts
	 * @return string
	 */
	public static function get( $atts ) {
		global $woocommerce;
		return $woocommerce->shortcode_wrapper( array( __CLASS__, 'output' ), $atts, array( "class" => "", 'before' => " ", 'after' => " " ) );
	}

	/**
	 * Output the shortcode.
	 *
	 * @access public
	 * @param array $atts
	 * @return void
	 */
	public static function output( $atts ) {
		global $woocommerce;
		
		extract( shortcode_atts( array(
			'title' => "",
			'font_size' => false,
			'color' => "#8e9396",
			'theme' => 'light'
		), $atts ) );
		
		$form_fields = get_option('shop-assistant-fields');
		$button_text = get_option('shop-assistant-button-text');
		if ( ( !isset($button_text) ) || ( $button_text == "" ) ) {
			$button_text = "Find Them!";
		}
		
		if (empty($form_fields)) {
			return;
		} else {
			$form_fields = unserialize($form_fields);
		}
		$i = 0;
		?>
		
		<div class="woocommerce-shop-assistant <?php if ( $theme != "none" ) echo "woocommerce-shop-assistant-themed woocommerce-shop-assistant-" . $theme ; else echo "woocommerce-shop-assistant-" . $theme ?>" <?php if($font_size) echo "style='font-size: " . $font_size . "px;'"; ?> >
			
			<?php if($title != ""): ?>
				<span class="assistant-title">
					<?php echo $title; ?>
				</span>
			<?php endif; ?>
			
			<span class="assistant-reset">
				<?php _e("Reset","shop-assistant"); ?>
			</span>
			
			<span class="assistant-phrase">
				<?php
				foreach ($form_fields as $form_field) {
					
					$text = $form_field["text"];
					$filter = $form_field["filter"];
					$priceone = $form_field["priceone"];
					$pricetwo = $form_field["pricetwo"];
					$label = $form_field["label"];
					$sanitized_label = sanitize_title($label);
					
					if ( $filter != "" ){
						?>
						
						<?php echo $text; ?> 
						
						<?php
						if ( $filter == "product_cat" ) {
							$woo_categories = get_terms( "product_cat", array( "hide_empty" => 1 ) );
							?>
							
							<?php if ($woo_categories) { ?>
								<span class="assistant-field assistant-field-select assistant-field-type-product-category" data-original-value="<?php echo $label; ?>" <?php if($color!="#8e9396") echo "style='color: $color;'" ; ?> >
									<a href "#"><?php echo $label; ?></a>
									<ul class="assistant-select">
										<li>
											<a href="#" data-value="any"><?php _e("Any", "shop-assistant"); ?></a>
										</li>
										<?php
										foreach ($woo_categories as $term ) { ?>
											<li <?php echo (sanitize_title($term->name) == $sanitized_label) ? 'class="selected original"' : ""; ?> >
												<a href "#" data-value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
											</li>
										<?php } ?>
									</ul>
								</span>
							<?php } ?>
							
							<?php
						}
						elseif ( $filter == "product_tag" ) {
							$woo_tags = get_terms( "product_tag", array( "hide_empty" => 1 ) );
							?>
							
							<?php if ($woo_tags) { ?>
								<span class="assistant-field assistant-field-select assistant-field-type-product-tag" data-original-value="<?php echo $label; ?>" <?php if($color!="#8e9396") { echo "style='color: $color;'" ; } ?> >
									<a href "#"><?php echo $label; ?></a>
									<ul class="assistant-select">
										<li>
											<a href="#" data-value="any"><?php _e("Any", "shop-assistant"); ?></a>
										</li>
										<?php foreach ($woo_tags as $term ) { ?>
											<li <?php echo (sanitize_title($term->name) == $sanitized_label) ? 'class="selected original"' : ""; ?> >
												<a href "#" data-value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
											</li>
										<?php } ?>
									</ul>
								</span> 
							<?php } ?>
							
							<?php
						}
						elseif ( $filter == "price" ) {
							?>
							
							<span class="assistant-field assistant-field-input" <?php if($color!="#8e9396") { echo "style='color: $color;'" ; } ?> >
								<span class="assistant-field-label"><?php echo get_woocommerce_currency_symbol(); ?></span>
								<input id="assistant-from-amount" name='sa_min_price' value="<?php echo $priceone; ?>" data-original-value="<?php echo $priceone; ?>">
							</span> 
							
							& 
							
							<span class="assistant-field assistant-field-input" <?php if($color!="#8e9396") { echo "style='color: $color;'" ; } ?> >
								<span class="assistant-field-label"><?php echo get_woocommerce_currency_symbol(); ?></span>
								<input id="assistant-to-amount" name='sa_max_price' value="<?php echo $pricetwo; ?>" data-original-value="<?php echo $pricetwo; ?>">
							</span> 
							
							<?php
						}
						else {
							$woo_attributes = $woocommerce->get_attribute_taxonomies();
							$filter_name = false;
							if ($woo_attributes) {
								foreach ($woo_attributes as $attribute ) {
									if ($filter == $attribute->attribute_id) {
										$filter_name = $attribute->attribute_name;
										break;
									}
								}
							}
							$attribute_taxonomy_name = $woocommerce->attribute_taxonomy_name( $filter_name );
							
							$terms = get_terms($attribute_taxonomy_name, array( "hide_empty" => 1 ) );
							?>
							
							<?php if ($terms) { ?>
								<span class="assistant-field assistant-field-select assistant-field-type-attribute" data-original-value="<?php echo $label; ?>" <?php if($color!="#8e9396") { echo "style='color: $color;'" ; } ?> >
									<a href "#"><?php echo $label; ?></a>
									<ul class="assistant-select" data-name="sa_filter_<?php echo $filter_name; ?>" >
										<li><a href="#" data-value="any"><?php _e("Any", "shop-assistant"); ?></a></li>
										<?php foreach ($terms as $term ) { ?>
											<li <?php echo (sanitize_title($term->name) == $sanitized_label) ? "class='selected'" : ""; ?>><a href "#" data-value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a></li>
										<?php } ?>
									</ul>
								</span> 
							<?php } ?>
							
							<?php
						}
						?>
						
						<?php
					}
					
				}
				?> 
				<span class="assistant-find"><?php echo $button_text; ?></span>
				
			</span>
			
		</div>
		
		<?php
	}

	
}