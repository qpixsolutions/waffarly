<?php	
class Repopulator {
	
	public static function get_template() {
		global $woocommerce;
		$woo_attributes = $woocommerce->get_attribute_taxonomies();
		$woo_categories = get_terms( "product_cat", array( "hide_empty" => 0 ) );
		$woo_tags = get_terms( "product_tag", array( "hide_empty" => 0 ) );
		
		$filter = "";
		
		$filter .= '<div class="field-group controls-row" id="field_group_{?}">';
		
		$filter .= '	<div class="field-group-set">';
		$filter .= '		<input type="text" class="shop-assistant-text" name="shop-assistant-fields[{?}][text]" value="{text}" id="text_{?}">';
		$filter .= '	</div>';
		
		$filter .= '	<div class="field-group-set">';
		$filter .= "		<select class='shop-assistant-filter' name='shop-assistant-fields[{?}][filter]' id='shop-assistant-fields[{?}][filter]'>";
		$filter .= "			<option value='' selected='selected' >" . __("Options", "shop-assistant") . "</option>";
		$filter .= "			<option value='price' >Price</option>";
		if ($woo_categories) {
			$filter .= "		<option value='product_cat' >" . __("Product Categories", "shop-assistant") . "</option>";
		}
		if ($woo_tags) {
			$filter .= "		<option value='product_tag' >" . __("Product Tags", "shop-assistant") . "</option>";
		}
		if ($woo_attributes) {
			foreach ($woo_attributes as $attribute ) {
				$filter .= "	<option value='".$attribute->attribute_id."' >".$attribute->attribute_label."</option>";
			}
		}
		$filter .= "		</select>";
		$filter .= "	</div>";
		
		$filter .= '	<div class="field-group-set price-set shop-assistant-hide">';
		$filter .= '		<span class="number-set">';
		$filter .= '			<input type="text" class="shop-assistant-priceone" name="shop-assistant-fields[{?}][priceone]" value="{priceone}" id="priceone_{?}">';
		$filter .= '		</span>';
		$filter .= '		<span class="number-set">';		
		$filter .= '			<input type="text" class="shop-assistant-pricetwo" name="shop-assistant-fields[{?}][pricetwo]" value="{pricetwo}" id="pricetwo_{?}">';
		$filter .= '		</span>';
		$filter .= '	</div>';
		
		$filter .= '	<div class="field-group-set label-set">';
		$filter .= '		<input type="text" class="shop-assistant-label" name="shop-assistant-fields[{?}][label]" value="{label}" id="label_{?}">';
		$filter .= '	</div>';
		
		$filter .= '	<span class="action-buttons delete">' . __("Delete", "shop-assistant") . '</span>';
		
		$filter .= '	<span class="action-buttons handle reorder">' . __("Reorder", "shop-assistant") . '</span>';
		
		
		$filter .= '</div>';
		
		return $filter;
	}

	public static function repopulate() {
		global $woocommerce;
		
		$form_fields = get_option('shop-assistant-fields');
		
		if (empty($form_fields)) {
			return;
		} else {
			$form_fields = unserialize($form_fields);
		}
		$i = 0;
		foreach ($form_fields as $form_field) {
			$formatted_template = preg_replace("/\{\?\}/", $i++, Repopulator::get_template());
			$filter_value = false;
			foreach ($form_field as $k => $v) {
				$formatted_template = preg_replace("/\{{$k}\}/", htmlentities($v, ENT_QUOTES), $formatted_template);
				if ($k == "filter") {
					$filter_value = $v;
				}
			}
			echo $formatted_template;
			$j = $i - 1;
			if ($filter_value == "price") {
				$woocommerce->add_inline_js("jQuery('#field_group_".$j."').find('.price-set').removeClass('shop-assistant-hide');");
				$woocommerce->add_inline_js("jQuery('#field_group_".$j."').find('.label-set').addClass('shop-assistant-hide');");
			}
			$woocommerce->add_inline_js("jQuery('#field_group_".$j."').find('.shop-assistant-filter').val('".$filter_value."');");
			
		}
	}
}