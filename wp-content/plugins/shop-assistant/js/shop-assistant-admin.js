jQuery( function($){

	$( document ).ready( function() {
		
		if ( $(".shop_assistant_sentence_builder").length ) {
			
			$(".shop_assistant_sentence_builder .repeatable").repeatable({
				addTrigger: ".shop_assistant_sentence_builder .add",
				deleteTrigger: ".shop_assistant_sentence_builder .delete",
				template: "#shop_assistant_sentence_builder",
				startWith: 1,
				onDelete: function() {
					build_search_phrase();
				}
			});
			
			$(".shop_assistant_sentence_builder").on("change", ".shop-assistant-filter", function() {
				if ($(this).val() == "price") {
					$(this).parents(".field-group:eq(0)").find(".price-set").removeClass('shop-assistant-hide');
					$(this).parents(".field-group:eq(0)").find(".label-set").addClass('shop-assistant-hide');
				} else {
					$(this).parents(".field-group:eq(0)").find(".price-set").addClass('shop-assistant-hide');
					$(this).parents(".field-group:eq(0)").find(".label-set").removeClass('shop-assistant-hide');
				}
			});
			
			$(".shop_assistant_sentence_builder .repeatable").sortable({ items: "> div.field-group", cursor: "move", stop: function( event, ui ) {
				build_search_phrase();
			}});
			$(".shop_assistant_sentence_builder .repeatable");
			
		}
		
		
		// Setup the tips
		jQuery(".tips, .help_tip").tipTip({attribute:"data-tip",fadeIn:50,fadeOut:50,delay:200, defaultPosition:"top"});
		
		build_search_phrase();
		
		$(".shop_assistant_sentence_builder .repeatable").on( "keyup", ".field-group .shop-assistant-text", build_search_phrase );
		$(".shop_assistant_sentence_builder .repeatable").on( "change", ".field-group .shop-assistant-filter", build_search_phrase );
		$(".shop_assistant_sentence_builder .repeatable").on( "keyup", ".field-group .shop-assistant-priceone", build_search_phrase );
		$(".shop_assistant_sentence_builder .repeatable").on( "keyup", ".field-group .shop-assistant-pricetwo", build_search_phrase );
		$(".shop_assistant_sentence_builder .repeatable").on( "keyup", ".field-group .shop-assistant-label", build_search_phrase );
		$(".shop-assistant-button-text").on( "keyup", build_search_phrase );
		
	});

	function build_search_phrase() {
		var output = "";
		
		$(".shop_assistant_sentence_builder .repeatable .field-group").each(function() {
			
			var text = $(this).find(".shop-assistant-text").val();
			
			if ( text != "" ) {
				output += text + " ";
			}
			
			var filter = $(this).find(".shop-assistant-filter").val();
			
			
			if ( filter != "" ) {
				
				var priceone = $(this).find(".shop-assistant-priceone").val();
				var pricetwo = $(this).find(".shop-assistant-pricetwo").val();
				var label = $(this).find(".shop-assistant-label").val();
				
				if ( filter == "price") {
					
					output += '<span class="phrase-example-filter">' + shop_assistant_params.currency_symbol + priceone + '</span> & <span class="phrase-example-filter">' + shop_assistant_params.currency_symbol + pricetwo + '</span> ';
					
				} else {
					
					output += '<span class="phrase-example-filter">' + label + "</span> ";
					
				}
				
			}
			
		});
		
		if (output != "") {
			output = output.substring(0, output.length - 1);
			
			var button_text = $(".shop-assistant-button-text").val();
			
			if ( button_text != "" ) {
				output += '. <span class="phrase-example-button">' + button_text + '</span>';
			} else {
				output += '. <span class="phrase-example-button">' + shop_assistant_params.default_button_text + '</span>';
			}
			
			$(".search-phrase").html(output);
			$(".search-phrase").show();
			$(".pre-search-phrase").hide();
		} else {
			$(".pre-search-phrase").show();
			$(".search-phrase").hide();
		}
		
	}
	
});