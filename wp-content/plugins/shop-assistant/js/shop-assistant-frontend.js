jQuery( function($){

	$( document ).ready( function() {
		
		$('.assistant-find').on("click", function() {
			var assistant = $(this).parents('.woocommerce-shop-assistant:eq(0)');
			update_search_link(assistant);
		});
		
	});
	
	function update_search_link(assistant) {
		var current_shop_url = shop_assistant_params.shop_url;
		var product_cats = "";
		var product_tags = "";
		var product_atts = "";
		
		var query = "?";
		if ( current_shop_url.indexOf("?") != -1 ) {
			query = "&";
		}
		
		// Product Categories
		if (assistant.find('.assistant-field-type-product-category').length) {
			assistant.find('.assistant-field-type-product-category').each(function() {
				if ( $(this).find(".assistant-select li.selected").length ) {
					if ( $(this).find(".assistant-select li:not(:first).selected a").attr("data-value") ) {
						product_cats += $(this).find(".assistant-select li.selected a").attr("data-value") + ",";
					}
				}
			});
			if (product_cats != "") {
				product_cats = product_cats.substring(0, product_cats.length - 1);
				query += "sa_product_cat=" + product_cats + "&";
			}
		}
		
		// Product Tags
		if (assistant.find('.assistant-field-type-product-tag').length) {
			assistant.find('.assistant-field-type-product-tag').each(function() {
				if ( $(this).find(".assistant-select li:not(:first).selected").length ) {
					product_tags += $(this).find(".assistant-select li.selected a").attr("data-value") + ",";
				}
			});
			if (product_tags != "") {
				product_tags = product_tags.substring(0, product_tags.length - 1);
				query += "sa_product_tag=" + product_tags + "&";
			}
		}
		
		// Product Min Price
		if (assistant.find('input[name="sa_min_price"]').length) {
			var sa_min_price = assistant.find('input[name="sa_min_price"]').val();
			query += "sa_min_price=" + sa_min_price + "&";
		}
		
		// Product Max Price
		if (assistant.find('input[name="sa_max_price"]').length) {
			var sa_max_price = assistant.find('input[name="sa_max_price"]').val();
			query += "sa_max_price=" + sa_max_price + "&";
		}
		
		// Product Attributes
		if (assistant.find('.assistant-field-type-attribute').length) {
			assistant.find('.assistant-field-type-attribute').each(function() {
				if ( $(this).find(".assistant-select li:not(:first).selected").length ) {
					var attribute_name = $(this).find(".assistant-select").attr("data-name");
					var attribute_value = $(this).find(".assistant-select li.selected a").attr("data-value");
					query += attribute_name + "=" + attribute_value + "&";
				}
			});
		}
		
		if ( ( query != "?" ) && ( query != "&" ) )  {
			query = query.substring(0, query.length - 1);
			current_shop_url += query;
			
			window.location = current_shop_url;
		} else {
			return false;
		}
		
	}
	
	
	//Fade in button
	jQuery(".woocommerce-shop-assistant").hover(
		function(event) {
			var this_assistant = jQuery(this);
			var find_button = jQuery(this).find(".assistant-find");
						
			if(!this_assistant.hasClass('assistant-has-activated')){
				
				// Animate the find button once
				find_button.stop(true,true).
				animate({top:"-8px",opacity:0,},100,'easeInOutQuad').
				animate({top:"8px",opacity:0},1).
				delay(100).
				animate({top:"0px",opacity:1},100,'easeInOutQuad');
				
				this_assistant.addClass('assistant-has-activated');
			}
			event.preventDefault();
		},
		function(event) {
			event.preventDefault();
		}
	);
	
	
	//Set Width's of DropDowns
	jQuery(".assistant-field ul").each(function() {
		jQuery(this).css({marginLeft: -jQuery(this).width()/2 });
	});
	
	
	//Easing Functions
	jQuery.easing.easeInQuad = function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	};
	jQuery.easing.easeOutQuad = function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	};
	jQuery.easing.easeInOutQuad = function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	};

	
	//DropDown Open
	jQuery('.assistant-field > a').on('click', function(event){
		event.preventDefault();
		var ullist = jQuery(this).parent().children('ul:first');
		
		ullist.slideDown(500,'easeInOutQuad').animate({top:"-20px"},{queue:false,duration:500,easing:'easeInOutQuad'});
		
		assistant_close_fields(  jQuery(".assistant-field").not(jQuery(event.target).parents(".assistant-field"))  );
		
		event.stopPropagation();
	});
	
	//DropDown Select
	jQuery('.assistant-field li a').on('click', function(event){
		event.preventDefault();
		
		var current_assistant = jQuery(this).parents('.woocommerce-shop-assistant');
		var current_field = jQuery(this).parents('.assistant-field');
		var current_field_display_text = current_field.find('a:first');
		var clicked_element = jQuery(this).parent('li');
		var other_elements = jQuery(this).parents("ul").find('li').not(clicked_element);
		var reset_element = current_assistant.find(".assistant-reset");
		
		//Set the class of the clicked element
		clicked_element.addClass("selected");
		other_elements.removeClass("selected");
		
		current_field.attr('data-new-value', clicked_element.find('a').html() );
		
		//Do the lock in animation
		current_field_display_text.
		delay(250).
		animate({top: "-10px",opacity:0},150).
		animate({top: "10px",opacity:0},1,function(){
			
			//Set the value of the top-most visible text
			current_field_display_text.html( current_field.attr('data-new-value') );
			
			//Set the value of the dropdown
			current_value = clicked_element.find('a').attr("data-value");
			if(current_value === "" || current_value == "any" || current_value === undefined)
				current_field.find('select').val("");
			else
				current_field.find('select').val(current_value);
			
		}).
		animate({top: "0px",opacity:1},150);
		
		//Close all fields on select of something
		assistant_close_fields(jQuery('.assistant-field'));
		
		//switch on the reset button
		current_assistant.addClass("assistant-active");
		
		event.stopPropagation();
	});
	
	//Close all on click outside
	jQuery(document).click(function(event) {
		//Close the feilds of all the assistants except the current one
		assistant_close_fields( jQuery(".assistant-field").not(jQuery(event.target).parents(".assistant-field")) );
	});
	
	//Function to close all
	function assistant_close_fields(elements){
		elements.each(function(){
			jQuery(this).find('ul').slideUp(300,'easeInOutQuad').animate({top:"0"},{queue:false,duration:300,easing:'easeInOutQuad'});
		});
	}
	
	//Clear Button Click
	jQuery('.woocommerce-shop-assistant .assistant-reset').on('click', function(event){
		
		var current_assistant = jQuery(this).parents('.woocommerce-shop-assistant');
		var reset_element = current_assistant.find("assistant-reset");
		
		current_assistant.find('.assistant-field').each(function(){
			
			if( jQuery(this).hasClass('assistant-field-select') ){
				
				if( jQuery(this).find('ul li.original').length ) {
					jQuery(this).find('ul li.original').find('a').click();
				}
				else {
					jQuery(this).find('ul li').first().find('a').click();
					jQuery(this).attr("data-new-value", jQuery(this).attr('data-original-value') );
				}
			}
			else if( jQuery(this).hasClass('assistant-field-input') ) {
				
				jQuery(this).find('input').each(function(){
					
					reset_value = jQuery(this).attr('data-original-value');
					jQuery(this).val(reset_value);
					
				});
				
			}
			
		});
		
		//switch on the reset button
		current_assistant.removeClass("assistant-active");
		
		event.stopPropagation();
	});
	
	
	
	
});