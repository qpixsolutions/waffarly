<?php
/**
 * Plugin Name: Woocommerce Shop Assistant
 * Description: Let your store help your customers like a real person with your own completely customizable search.
 * Author: cxThemes
 * Author URI: http://codecanyon.net/user/cxThemes
 * Plugin URI: http://codecanyon.net/item/shop-assistant-for-woocommerce/6644090
 * Version: 1.04
 * Text Domain: shop-assistant
 * Domain Path: /languages/
 *
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   Shop-Assistant
 * @author    cxThemes
 * @category  WooCommerce
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * Required functions
 **/
if ( ! function_exists( 'is_woocommerce_active' ) ) require_once( 'woo-includes/woo-functions.php' );

require 'plugin-updates/plugin-update-checker.php';
$ShopAssistantUpdateChecker = new PluginUpdateChecker(
	'http://cxthemes.com/plugins/woocommerce-shop-assistant/shop-assistant.json',
	__FILE__,
	'shop-assistant'
);

if ( ! is_woocommerce_active() ) return;

global $plugin_dir_path_shop_assistant;

$plugin_dir_path_shop_assistant = basename( plugin_dir_path( __FILE__ ) );

/**
 * The Shop_Assistant global object
 * @name $shop_assistant
 * @global Shop_Assistant $GLOBALS['shop_assistant']
 */
$GLOBALS['shop_assistant'] = new Shop_Assistant();

/**
 * Email Cart Main Class.  This class is responsible
 * for setting up the admin start page/menu
 * items.
 *
 */
class Shop_Assistant {

	private $id = 'shop_assistant';

	/** Plugin text domain */
	const TEXT_DOMAIN = 'shop-assistant';

	/**
	 * Construct and initialize the main plugin class
	 */
	public function __construct() {
		
		add_action( 'init',    array( $this, 'load_translation' ) );
		
		// add the menu itme
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );
		
		$this->general_includes();
		
		if ( ( ! empty($_GET["page"]) ) && ( $_GET["page"] == "shop_assistant" )  ) {
			
			add_action( 'woocommerce_init', array( $this, 'backend_includes' ) );
			
			add_action( 'admin_init', array( $this, 'shop_assistant_save_options' ) );
			
		}
			
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		
		
		if ( ! is_admin() ) {
			
			add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
	
		}
		
		add_shortcode( 'shop_assistant', array( $this, 'shortcode_shop_assistant' ) );
		
		add_filter( 'pre_get_posts', array( $this, 'shop_assistant_search_query' ) );
		
		add_filter( 'loop_shop_post_in', array( $this, 'woocommerce_shop_assistant_price_filter' ) );
	}
	
	
	/**
	 * Localization
	 */
	public function load_translation() {
		// localisation
		load_plugin_textdomain( 'shop-assistant', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}


	/**
	 * Add a submenu item to the WooCommerce menu
	 */
	public function admin_menu() {
		
		add_submenu_page( 'woocommerce',
						  __( 'Shop Assistant', 'shop-assistant' ),
						  __( 'Shop Assistant', 'shop-assistant' ),
						  'manage_woocommerce',
						  $this->id,
						  array( $this, 'admin_page' ) );

	}
	
	/**
	 * Include backend required files.
	 *
	 * @return void
	 */
	public function backend_includes() {				// Contains functions
		// Repopulator Class
		include_once( 'classes/class-repopulator.php' );
		
		// Functions Class
		include_once( 'admin/functions.php' );
		
	}
	
	/**
	 * Include general required files.
	 *
	 * @return void
	 */
	public function general_includes() { // Contains functions
		
		// Shortcode Class
		include_once( 'classes/shortcodes/class-shortcode-shop-assistant.php' );
		
		// Widget Class
		include_once( 'classes/widgets/class-widget-shop-assistant.php' );
		
	}


	/**
	 * Include admin scripts
	 */
	public function admin_scripts($hook) {
		global $woocommerce, $wp_scripts;
		
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		
		if ( ( ( ! empty($_GET["page"]) ) && ( $_GET["page"] == "shop_assistant" ) ) || ( $hook == "widgets.php" ) ) {
			
			wp_enqueue_style( 'wp-color-picker' );
			
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'jquery-ui-widget' );
			wp_enqueue_script( 'jquery-ui-mouse' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			
			wp_enqueue_style( 'woocommerce_admin_styles', $woocommerce->plugin_url() . '/assets/css/admin.css' );
			
			wp_register_style( 'shop-assistant', plugins_url( basename( plugin_dir_path( __FILE__ ) ) . '/css/admin-style.css', basename( __FILE__ ) ), '', '1.04', 'screen' );
			wp_enqueue_style( 'shop-assistant' );
			
			wp_register_script( 'shop-assistant-repeatable', plugins_url( basename( plugin_dir_path( __FILE__ ) ) . '/js/jquery.repeatable.js', basename( __FILE__ ) ), array('jquery') );
			wp_enqueue_script( 'shop-assistant-repeatable' );
			
			wp_register_script( 'shop-assistant', plugins_url( basename( plugin_dir_path( __FILE__ ) ) . '/js/shop-assistant-admin.js', basename( __FILE__ ) ), array('jquery', 'wp-color-picker'), true );
			wp_enqueue_script( 'shop-assistant' );
			
			wp_register_script( 'jquery-tiptip', $woocommerce->plugin_url() . '/assets/js/jquery-tiptip/jquery.tipTip' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
			wp_enqueue_script( 'jquery-tiptip' );
			
			$shop_assistant_params = array(
				'currency_symbol'				=> get_woocommerce_currency_symbol(),
				'default_button_text'				=> __("Find Them!", "shop-assistant")
			);
			
			wp_localize_script( 'shop-assistant', 'shop_assistant_params', $shop_assistant_params );
			
		}
		
	}
	
	
	public function frontend_scripts() {
		global $woocommerce, $wp_scripts;
		
		if ( get_option("permalink_structure") == "" ) {
			$shop_url = get_post_type_archive_link( 'product' );
		} else {
			$shop_url = get_permalink( get_option( 'woocommerce_shop_page_id' ) );
		}
		
		$shop_assistant_params = array(
			'shop_url' 	=> $shop_url
		);
		
		wp_register_script( 'shop-assistant-frontend', plugins_url( basename( plugin_dir_path( __FILE__ ) ) . '/js/shop-assistant-frontend.js', basename( __FILE__ ) ), array('jquery'), '1.04', true );
		wp_enqueue_script( 'shop-assistant-frontend' );
		
		wp_localize_script( 'shop-assistant-frontend', 'shop_assistant_params', $shop_assistant_params );
		
		wp_register_style( 'shop-assistant-frontend', plugins_url( basename( plugin_dir_path( __FILE__ ) ) . '/css/frontend-style.css', basename( __FILE__ ) ), '', '1.04', 'screen' );
		wp_enqueue_style( 'shop-assistant-frontend' );
			
		wp_localize_script( 'shop-assistant-frontend', 'shop_assistant_params', $shop_assistant_params );
		
	}
	
	public function register_widgets() {
		
		register_widget( 'WC_Widget_Shop_Assistant' );
		
	}


	/**
	 * Render the admin page
	 */
	public function admin_page() {
		
		global $woocommerce;
		
		$action = 'admin.php?page=shop_assistant'; ?>
		
		<div class="shop-assistant-wrap wrap">
			
			<div class="icon32"><br></div>
			<h2><?php echo __('Shop Assistant', 'shop-assistant' ); ?></h2>
			
			<form action="<?php echo esc_attr( $action ); ?>" method="POST">
				<div id="poststuff">
					
					<div id="woocommerce-order-items" class="postbox " >
						
						<!--
						<div class="handlediv" title="Click to toggle">
							<br />
						</div>
						
						<h3 class='hndle'>
							<span>&nbsp;</span>
						</h3>
						-->
						
						<div class="inside">
							
							<table class="settings-table">
								<tbody>
									
									<tr>
										<td class="label">

											<label><?php _e('Shop Assistant Search Phrase', 'shop-assistant'); ?></label>
											<p class="description"><?php _e( 'Construct your search phrase here. Add as many or as few search parts as you like. Connect parts together with natural language using the Text field.', 'shop-assistant' ); ?></p>

										</td>
										<td>
											<div class="cxsc-settings-blocks">
												
												<div class="phrase-example-holder">
													<div class="phrase-example">
														
														<div class="search-phrase"></div>
														
														<div class="phrase-example-none pre-search-phrase">
															<?php _e( 'Start building your search phrase by adding your first part below...', 'shop-assistant' ); ?>
														</div>
													</div>
													
													<div class="help-tip">
														<?php _e( 'This is an example of what your search phrase will look like. (All options are inactive in this example)', 'shop-assistant' ); ?>
													</div>
												</div>
												
												<div class="cxsc-settings-block" id="cxsc-settings-block-general">
													<?php shop_assistant_options_field_display( 'filter' ); ?>
												</div>
											</div>
										</td>
									</tr>
									
									<tr>
										<td class="label">

											<label><?php _e('Button Text', 'shop-assistant'); ?></label>
											<p class="description"><?php _e('This is the search button text at the end of your search phrase.', 'shop-assistant' ); ?></p>
										</td>
										<td>
											<div class="second-block">
												<?php
												$shop_assistant_button_text = get_option('shop-assistant-button-text');
												if ( ( !isset($shop_assistant_button_text) ) || ( $shop_assistant_button_text == "" ) ) {
													$shop_assistant_button_text = __("Find Them!", "shop-assistant");
												} ?>
												<input type="text" class="shop-assistant-button-text" name="shop-assistant-button-text" id="shop-assistant-button-text" value="<?php echo htmlentities($shop_assistant_button_text, ENT_QUOTES); ?>">
											</div>
										</td>
									</tr>
									
									<tr>
										<td class="label">
											<label><?php _e('Add Shop Assistant to your site', 'shop-assistant'); ?></label>
											<p class="description"></p>
										</td>
										<td>
											<div class="second-block">
												<p class="description"><?php _e( 'There are three ways to show the Shop Assistant search phrase to your site. <br /><br />1) By Widget <br />2) In your template code: do_shortcode("[shop_assistant]") <br />3) Add the shortcode: [shop_assistant] to your page content <br /><br />For the shortcode you can also use the following attributes: "title", "color" (hex value), "theme" (light, dark, none), "font_size"<br /><br />eg. [shop_assistant title="Let out Shop Assistant help find what you\'re looking for!" color="#bb1930" theme="light" font_size="26" ]', 'shop-assistant' ); ?></p>
											</div>
										</td>
									</tr>
									
									<tr>
										<td class="label">
										</td>
										<td>
											<input type="hidden" name="action" value="shop-assistant-submitted" />
											<input type="submit" class="button button-primary submit-button" name="submit" id="submit" value="<?php _e( 'Save Settings', 'shop-assistant' ); ?>" />
										</td>
									</tr>
									
								</tbody>
							</table>
							
						</div>
					</div>
				
				</div>
				<?php $woocommerce->nonce_field('shop-assistant'); ?>
			</form>
			
			
		</div>
		
		
		<?php
		
	}
	
	/**
	 * Create shortcode for Shop Assistant
	 */
	public function shortcode_shop_assistant( $atts ) {
		
		global $woocommerce;
		return $woocommerce->shortcode_wrapper( array( 'WC_Shortcode_Shop_Assistant', 'output' ), $atts );
		
	}
	
	/**
	 * Save Options
	 */
	
	function shop_assistant_save_options(){
		
		global $woocommerce;
			
		$woocommerce->verify_nonce( 'shop-assistant' );
		
		if ( isset($_POST["shop-assistant-fields"]) ) {
			
			$filter_fields = $_POST["shop-assistant-fields"];
			if ($filter_fields) {
				$formatted_filter_fields = array();
				foreach ($filter_fields as $filter_field) {
					if ( $filter_field["filter"] != "" ) {
						$formatted_filter_fields[] = stripslashes_deep(array_map("woocommerce_clean", $filter_field));
					}
				}
				$serialized_filter_fields = serialize($formatted_filter_fields);
				
				update_option('shop-assistant-fields', $serialized_filter_fields );
				
			}
		} else {
			if ( ( $_GET["page"] == "shop_assistant" ) && ( isset( $_POST["action"] ) ) ) {
				delete_option( 'shop-assistant-fields' );
			}
		}
		
		if ( isset($_POST["shop-assistant-button-text"]) ) {
			$button_text = stripslashes(woocommerce_clean($_POST["shop-assistant-button-text"]));
			update_option('shop-assistant-button-text', $button_text );
		}
	}
	
	function shop_assistant_search_query($query) {
		global $woocommerce, $wp_query, $wpdb;
		
		if ( !is_admin() && $query->is_main_query() ) {	
			
			$woocommerce_current_page_id = ( version_compare( $woocommerce->version, '2.1', '<' ) ) ? woocommerce_get_page_id('shop') : wc_get_page_id('shop');
			
			if ( is_shop() || ( $query->is_page() && 'page' == get_option( 'show_on_front' ) && $query->get('page_id') == $woocommerce_current_page_id ) ) {

				$product_categories = ( isset( $_GET['sa_product_cat'] ) ) ? $_GET['sa_product_cat'] : null;
				$product_tags = ( isset( $_GET['sa_product_tag'] ) ) ? $_GET['sa_product_tag'] : null;
				
				$tax_query = false;
				
				// Product Attributes
				$attribute_taxonomies = $woocommerce->get_attribute_taxonomies();
				$selected_attributes = false;
				
				if ( $attribute_taxonomies ) {
					
					foreach ( $attribute_taxonomies as $tax ) {

				    	$attribute = sanitize_title( $tax->attribute_name );
				    	$taxonomy = $woocommerce->attribute_taxonomy_name( $attribute );

						// create an array of product attribute taxonomies
						$_attributes_array[] = $taxonomy;

				    	$name = 'sa_filter_' . $attribute;

				    	if ( ! empty( $_GET[ $name ] ) && taxonomy_exists( $taxonomy ) ) {
				    		
				    		$selected_attributes[ $taxonomy ]['terms'] = $_GET[ $name ];

						}
					}
					
					if ( $selected_attributes ) {
						
						foreach ($selected_attributes as $key => $value) {
							
							$tax_query[] = array(
								'taxonomy' => $key,
								'field' => 'id',
								'terms' => $value["terms"],
								'operator' => 'IN'
							);
							
						}
						
					}
			    }
			    
			    // Product Categories
			    if ( ( isset($product_categories) ) && ( $product_categories != "" ) ) {
					
					$tax_query[] = array(
						'taxonomy'     => 'product_cat',
						'field'        => 'id',
						'terms'        => $product_categories,
						'operator' => 'IN'
					);
					
				}
				
				// Product Tags
			    if ( ( isset($product_tags) ) && ( $product_tags != "" ) ) {
					
					$tax_query[] = array(
						'taxonomy'     => 'product_tag',
						'field'        => 'id',
						'terms'        => $product_tags,
						'operator' => 'IN'
					);
					
				}
			    
			    if ( $tax_query ) {
			    	add_filter( "woocommerce_page_title", array( $this, "woocommerce_shop_assistant_title" ) );
					$tax_query['relation'] = 'AND';
					$query->set('tax_query', $tax_query  );
						
				}
				
			}
		}
		return $query;
	}
	
	function woocommerce_shop_assistant_title( $title ) {
		
		return __( "Search Results:", "shop-assistant" );
		
	}
	
	/**
	 * Price Filter post filter
	 */
	function woocommerce_shop_assistant_price_filter($filtered_posts) {
	    global $wpdb;

	    if ( isset( $_GET['sa_max_price'] ) && isset( $_GET['sa_min_price'] ) ) {

	        $matched_products = array();
	        $min 	= floatval( $_GET['sa_min_price'] );
	        $max 	= floatval( $_GET['sa_max_price'] );

	        $matched_products_query = $wpdb->get_results( $wpdb->prepare("
	        	SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
				INNER JOIN $wpdb->postmeta ON ID = post_id
				WHERE post_type IN ( 'product', 'product_variation' ) AND post_status = 'publish' AND meta_key = %s AND meta_value BETWEEN %d AND %d
			", '_price', $min, $max ), OBJECT_K );

	        if ( $matched_products_query ) {
	            foreach ( $matched_products_query as $product ) {
	                if ( $product->post_type == 'product' )
	                    $matched_products[] = $product->ID;
	                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
	                    $matched_products[] = $product->post_parent;
	            }
	        }

	        // Filter the id's
	        if ( sizeof( $filtered_posts ) == 0) {
	            $filtered_posts = $matched_products;
	            $filtered_posts[] = 0;
	        } else {
	            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
	            $filtered_posts[] = 0;
	        }

	    }
	    return (array) $filtered_posts;
	}

} // class Shop_Assistant
