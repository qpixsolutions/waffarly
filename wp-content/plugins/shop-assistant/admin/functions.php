<?php
/**
 * Sanitize the other options fields
 *
 * @param $values
 * @return mixed
 */
function shop_assistant_options_sanitize_display($values){
    // if necessary do this
    //$values = unserialize($values);
    return $values;
}

/**
 * Display the fields for the other settings.
 *
 * @param $args
 */
function shop_assistant_options_field_display($type){
    
    switch($type) {
        
        case 'input' :
            ?><input type="text" name="shop-assistant-settings[<?php echo esc_attr($args['type']) ?>]" value="<?php echo esc_attr($settings[$args['type']]) ?>" class="regular-text" /><?php
            break;
        case 'small-input' :
            ?><input type="text" name="shop-assistant-settings[<?php echo esc_attr($args['type']) ?>]" value="<?php echo esc_attr($settings[$args['type']]) ?>" class="small-text" /> <?php _e('px', 'shop-assistant') ?><?php
            break;
        case 'filter' :
            global $woocommerce; ?>
            
            <fieldset class="shop_assistant_sentence_builder">
            	<div class="header-row">

                    <div class="header-column header-column-1"><?php _e("Text", "shop-assistant"); ?> <a class="header-help help_tip" data-tip="<?php _e( 'Connect parts of the search phrase with natural language text.', 'shop-assistant' ); ?>" href="#">[?]</a></div>
                    <div class="header-column header-column-2"><?php _e("Option", "shop-assistant"); ?> <a class="header-help help_tip" data-tip="<?php _e( 'The Option could be Price, Category, etc. The user uses the options to complete the phrase to the find their products.', 'shop-assistant' ); ?>" href="#">[?]</a></div>
                    <div class="header-column header-column-3"><?php _e("Placeholder", "shop-assistant"); ?> <a class="header-help help_tip" data-tip="<?php _e( 'Input what is displayed before the user selects an option. Use the exact Option value to have the search phrase work without the user having to change the Option to another value.', 'shop-assistant' ); ?>" href="#">[?]</a></div>
                    <div style="clear:both;"></div>
                </div>
                <div class="repeatable">
                    <?php Repopulator::repopulate(); ?>
                </div>
                <div class="form-group span4">
                    <input type="button" value="<?php _e("Add New Part", "shop-assistant"); ?>" class="btn button btn-default add" />
                </div>
            </fieldset>
            
            <script type="text/template" id="shop_assistant_sentence_builder">
                <?php echo Repopulator::get_template(); ?>
            </script>
            <?php
            break;
    }
}

/**
 * Get the settings
 *
 * @param string $key Only get a specific key.
 * @return mixed
 */
function shop_assistant_setting($key = ''){
    $display_settings = false;
    switch($key) {
        case 'shop-assistant-fields' :
            $display_settings = get_option('shop-assistant-fields');
            
            if($display_settings) {
                $display_settings = unserialize($display_settings);
            }
            break;
    }
    

    return $display_settings;
}

