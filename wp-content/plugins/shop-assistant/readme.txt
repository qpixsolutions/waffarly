=== Plugin Name ===
Contributors: cxThemes
Tags: woocommerce, search, filter, category, navigation, product, find, advanced search, tag, personal shopper, virtual shopper, widget, ui, user interface
Requires at least: 3.0.1
Tested up to: 3.6
Stable tag: 1.04
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Let your store help your customers like a real person with your own completely customizable search.

== Description ==

What it Does
The more quickly and easily a customer can find the products they want in your store, the more they will buy and the happier they will be with their experience.
Shop Assistant offers your customers a natural language search function where they complete a highly specific search tailored to their needs in the simplest way possible.
No more filtering over and over to find the products they want, or struggling to navigate complicated product menu trees, or filling out long advanced search forms. Shop Assistant allows your customer to create a natural language search 'sentence' quickly and easily from anywhere in your site, which feels just like asking a real person for help, leading directly to the products they are after.
The sentence is completely customisable and as broad or as specific as you choose and can be placed anywhere on any page, any widgetized area, or directly in your template code.

Features
* Let your store help your customers like a real person
* Create your own search sentence
* Highly specific or simple and broad
* Effective, fast, easy and natural for your customers
* Increases sales

Happy Conversions!


== Documentation ==

Please see the included PDF for full instructions on how to use this plugin.

== Changelog ==

= 1.04 =
* Fixed bug when plugin appears on shop-page set to front-page

= 1.03 =
* Added Clean Theme and made None have less styling
* Changed UI so that Reset only shows after a change is made
* Updated UpdateChecker class
* Removed surrounding div on shortcode output
* Various small bug fixes

= 1.02 =
* Fixed bug stopping draggable ui on admin page

= 1.01 =
* Fixed the query on the search results page
* Fixed bug causing loss of search phrase on the admin page after saving
* Changed taxonomies and attributes to exclude empty terms

= 1.0 =
* Initial release
