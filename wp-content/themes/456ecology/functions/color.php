<?php function lpd_color_styles() {?>

<?php
$theme_color_1 = ot_get_option('theme_color');
$theme_color_2 = ot_get_option('theme_color_2');

if($theme_color_1==""){
	$theme_color_1 = "#79ad4b";
}

if($theme_color_2==""){
	$theme_color_2 = "#70a145";
}

?>

<?php if($theme_color_1||$theme_color_2){?>
<style>
a {
  color: <?php echo $theme_color_1;?>;
}
.text-primary {
  color: <?php echo $theme_color_1;?>;
}
.btn-primary {
  background-color: <?php echo $theme_color_1;?>;
  border-color: <?php echo $theme_color_1;?>;
}
.btn-primary.disabled,
.btn-primary[disabled],
fieldset[disabled] .btn-primary,
.btn-primary.disabled:hover,
.btn-primary[disabled]:hover,
fieldset[disabled] .btn-primary:hover,
.btn-primary.disabled:focus,
.btn-primary[disabled]:focus,
fieldset[disabled] .btn-primary:focus,
.btn-primary.disabled:active,
.btn-primary[disabled]:active,
fieldset[disabled] .btn-primary:active,
.btn-primary.disabled.active,
.btn-primary[disabled].active,
fieldset[disabled] .btn-primary.active {
  background-color: <?php echo $theme_color_1;?>;
  border-color: <?php echo $theme_color_1;?>;
}
.btn-primary .badge {
  color: <?php echo $theme_color_1;?>;
}
.btn-link {
  color: <?php echo $theme_color_1;?>;
}
.btn-link:hover,
.btn-link:focus {
  color: <?php echo $theme_color_1;?>;
}
.label-primary {
  background-color: <?php echo $theme_color_1;?>;
}
.header-bottom-wrap{
	background-color: <?php echo $theme_color_1;?>;
}
.header-content a:hover{
	color: <?php echo $theme_color_1;?>;
}
.lpd_breadcrumb a:hover{
	color: <?php echo $theme_color_1;?>;
}
.footer-meta{
	background-color: <?php echo $theme_color_1;?>;
}
.footer.dark-theme .widget.widget_rss ul li a:hover,
.footer.dark-theme .widget.widget_pages ul li a:hover,
.footer.dark-theme .widget.widget_nav_menu ul li a:hover,
.footer.dark-theme .widget.widget_login ul li a:hover,
.footer.dark-theme .widget.widget_meta ul li a:hover,
.footer.dark-theme .widget.widget_categories ul li a:hover,
.footer.dark-theme .widget.widget_archive ul li a:hover,
.footer.dark-theme .widget.widget_recent_comments ul li a:hover,
.footer.dark-theme .widget.widget_recent_entries ul li a:hover{
	color: <?php echo $theme_color_1;?>;
}
.blog-post-title a:hover{
	color: <?php echo $theme_color_1;?>;
}
.cbp-popup-singlePage .cbp-popup-navigation-wrap{
	background-color: <?php echo $theme_color_1;?>;
}
.mega-icon-bg{
	background-color: <?php echo $theme_color_1;?>;
}
.lpd-portfolio-item .title a:hover{
	color: <?php echo $theme_color_1;?>;
}
.pn-btn:hover{
	color: <?php echo $theme_color_1;?>;
}
.wpml-switcher-mobile .flag a:hover{
	color: <?php echo $theme_color_1;?>;
}
.lpd-shopping-cart a.cart-total:hover{
	color: <?php echo $theme_color_1;?>;
}
.menu3dmega > ul > li.open > a,
.menu3dmega > ul > li:hover > .arrow-icon-bottom:after, 
.menu3dmega > ul > li:hover > a,
.menu3dmega > ul > li:hover > a:after,
.menu3dmega > ul > li:hover > .arrow-icon-bottom{
}
.menu3dmega > ul ul li > a.halflings:hover:before{
	color: <?php echo $theme_color_1;?>;
}
.lpd_breadcrumb a:hover{
	color: <?php echo $theme_color_1;?>;
}
.widget.widget_pages ul li a:hover:before,
.widget.widget_nav_menu ul li a:hover:before,
.widget.widget_login ul li a:hover:before,
.widget.widget_meta ul li a:hover:before,
.widget.widget_categories ul li a:hover:before,
.widget.widget_archive ul li a:hover:before,
.widget.widget_recent_comments ul li a:hover:before,
.widget.widget_recent_entries ul li a:hover:before,
.widget.widget_rss ul li a:hover,
.widget.widget_pages ul li a:hover,
.widget.widget_nav_menu ul li a:hover,
.widget.widget_login ul li a:hover,
.widget.widget_meta ul li a:hover,
.widget.widget_categories ul li a:hover,
.widget.widget_archive ul li a:hover,
.widget.widget_recent_comments ul li a:hover,
.widget.widget_recent_entries ul li a:hover{
	color: <?php echo $theme_color_1;?>;
}
.tagcloud a:hover,
.tags a:hover{
	border-color: <?php echo $theme_color_1;?>;
	background-color: <?php echo $theme_color_1;?>; 
}
.blog-post-title a:before{
	background: <?php echo $theme_color_1;?>;
}
.single-post-meta a:hover,
.blog-post-meta a:hover{
	color: <?php echo $theme_color_1;?>;
}
.theme-option-contacts a:hover{
	color: <?php echo $theme_color_1;?>;
}
.meta-menu a:hover{
	color: <?php echo $theme_color_1;?>;
}
.header-middle-search .search-btn{
	background-color:<?php echo $theme_color_1;?>;
}
.footer-menu li a:hover{
	color: <?php echo $theme_color_1;?>;
}
.lpd-prodcut-accordion .panel-title a.halflings:before{
	color: <?php echo $theme_color_1;?>;
}
.lpd-cart-list-title a:hover{
	color: <?php echo $theme_color_1;?>;
}
.woocommerce .widget_layered_nav ul li a:hover,
.woocommerce-page .widget_layered_nav ul li a:hover{
	color: <?php echo $theme_color_1;?> !important;
}
.wordpress-456ecology .widget_product_categories ul li a:hover{
	color: <?php echo $theme_color_1;?> !important;
}
.vc_lpd_testiomonial .testiomonial_content:hover:before{
	border-top-color: <?php echo $theme_color_1;?>;
}
.meta-block:hover,
.vc_lpd_testiomonial .testiomonial_content:hover{
	border-color: <?php echo $theme_color_1;?>;
}
.vc_lpd_testiomonial .testiomonial_content:hover{
	color: <?php echo $theme_color_1;?>;
}
.meta-block:hover .sep-border{
	background-color: <?php echo $theme_color_1;?>; 
}
blockquote:hover{
	color: <?php echo $theme_color_1;?>;
	border-color: <?php echo $theme_color_1;?>;
}
.callout:hover{
	border-color: <?php echo $theme_color_1;?>;
}
.callout:hover .sep-border{
	background: <?php echo $theme_color_1;?>;
}
.title-404{
	color:  <?php echo $theme_color_1;?>;
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.open .dropdown-toggle.btn-primary {
	background-color: <?php echo $theme_color_2;?>;
	border-color: <?php echo $theme_color_2;?>;
}
.dropcap{
	background: <?php echo $theme_color_1;?>;
}
.lpd-product-thumbnail .loop-shop-btn{
	background-color: <?php echo $theme_color_1;?>;
}
.woocommerce .star-rating span,
.woocommerce-page .star-rating span{
	color: <?php echo $theme_color_1;?>;
}
.wordpress-456ecology .woocommerce ul.cart_list li a:hover,
.wordpress-456ecology .woocommerce ul.product_list_widget li a:hover,
.wordpress-456ecology.woocommerce-page ul.cart_list li a:hover,
.wordpress-456ecology.woocommerce-page ul.product_list_widget li a:hover{
	color: <?php echo $theme_color_1;?> !important;
}
/* cubeportfolio */
.cbp-l-filters-button .cbp-filter-counter{
	background-color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-button .cbp-filter-counter:before{
	border-top-color: <?php echo $theme_color_1;?>;
}
.cbp-popup-singlePage .cbp-l-project-desc-title span,
.cbp-popup-singlePage .cbp-l-project-details-title span{
	border-bottom-color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignCenter .cbp-filter-item:hover {
    color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignCenter .cbp-filter-item-active {
    color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignCenter .cbp-filter-counter{
    background: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignCenter .cbp-filter-counter:before{
	border-top-color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignRight .cbp-filter-counter:before{
	border-top-color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-alignRight .cbp-filter-counter{
    background: <?php echo $theme_color_1;?>;
}
.cbp-l-grid-blog-comments:hover{
    color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-list .cbp-filter-item-active{
    background-color: <?php echo $theme_color_1;?>;
}
.cbp-l-filters-list .cbp-filter-item{
	border-color: <?php echo $theme_color_1;?>;
}
/* cubeportfolio */
.cbp-l-filters-alignLeft .cbp-filter-item-active {
    /* @editable properties */
    background-color: <?php echo $theme_color_2;?>;
    border-color: <?php echo $theme_color_2;?>;
}
.cbp-l-filters-dropdownWrap {
    background: <?php echo $theme_color_2;?>;
}
</style>
<?php }?>

<?php }?>
<?php add_action( 'wp_head', 'lpd_color_styles' );?>