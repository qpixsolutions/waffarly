<?php function lpd_fonts_styles() {
	
	require_once(ABSPATH .'/wp-admin/includes/plugin.php');
	
?>

<?php $body_font_family = ot_get_option('body_font_family');
	
	if ($body_font_family==""){
		$body_font_family = "Lato";
	}
?>

<?php $heading_font_family = ot_get_option('heading_font_family');
	
	if ($heading_font_family==""){
		$heading_font_family = "Lato";
	}
?>

<?php $elements_font_family = ot_get_option('elements_font_family');
	
	if ($elements_font_family==""){
		$elements_font_family = "Raleway";
	}
?>

<?php $body_font_size  = ot_get_option('body_font_size');
	
	if ($body_font_size == ""){
		$body_font_size  = "13";
	}
?>

<?php $navigation_font_size  = ot_get_option('navigation_font_size');
	
	if ($navigation_font_size == ""){
		$navigation_font_size  = "13";
	}
?>

<?php $dropdown_font_size  = ot_get_option('dropdown_font_size');
	
	if ($dropdown_font_size == ""){
		$dropdown_font_size  = "13";
	}
?>

<?php $navigation_font_style  = ot_get_option('navigation_font_style');
	
	if ($navigation_font_style == ""){
		$navigation_font_style  = "700";
	}
?>

<?php $dropdown_font_style  = ot_get_option('dropdown_font_style');
	
	if ($dropdown_font_style == ""){
		$dropdown_font_style  = "normal";
	}
?>

<?php $bold_elements_font_style  = ot_get_option('bold_elements_font_style');
	
	if ($bold_elements_font_style == ""){
		$bold_elements_font_style  = "900";
	}
?>

<style>
.mm3_title,
.mm1_title,
.wordpress-456ecology .woocommerce nav.woocommerce-pagination ul li a,
.wordpress-456ecology.woocommerce-page nav.woocommerce-pagination ul li a,
.wordpress-456ecology .woocommerce nav.woocommerce-pagination ul li span,
.wordpress-456ecology.woocommerce-page nav.woocommerce-pagination ul li span,
.woocommerce .widget_layered_nav ul li a,
.woocommerce-page .widget_layered_nav ul li a,
.woocommerce .widget_layered_nav ul li span,
.woocommerce-page .widget_layered_nav ul li span,
.total-cart-wrap h2,
#order_review_heading,
.lpd-checkout-accordion .panel-title a,
.vc-lpd-woocommerce h2,
.lpd-related h2,
.lpd-upsells h2,
ul.lpd-products .product.product-category .product-category-title,
.wordpress-456ecology .vc_separator h4,
.lpd-cart-list-title a,
.lpd-shopping-cart .lpd-cart-total span,
.mega_header h2,
.lpd_title h4,
.vc_callout2 .callout2_title span,
.vc_callout2 .callout2_sep_word span,
.callout-content h4,
.iconitem h5.title,
.meta-block h4,
.vc_lpd_testiomonial .testiomonial_content .tc_title,
.lpd-module .module_content h3,
.featured-module-3-content h3,
.featured-module-3-content .featured-module-3-label,
.featured-module-2-content-2 h3,
.featured-module-2-content h3,
featured-module-1-content h3,
.working-time ul li span,
.widget .title,
.comment-author,
#title-breadcrumb h2,
.lpd-heading-title{
	font-weight: <?php echo $bold_elements_font_style;?>;
}
.menu3dmega > ul > li > a, .menu3dmega > ul > li > span{
	font-weight: <?php echo $navigation_font_style;?>;
	font-size: <?php echo $navigation_font_size;?>px;
}
.menu3dmega > ul ul li > a{
	font-weight: <?php echo $dropdown_font_style;?>;
	font-size: <?php echo $dropdown_font_size;?>px;
}
.form-control,
body{
	font-size: <?php echo $body_font_size;?>px;
}
.search-dropdown form input,
.lpd_breadcrumb,
p.copyright,
.btn,
.tagcloud a,
.tags a,
.blog-post-title,
.blog-post-taxo,
.blog-post-meta,
.single-post-meta,
.comment-info,
.portfolio-post-details ul,
.cbp-l-project-details-list,
.cbp-popup-singlePage .cbp-l-project-subtitle,
.cbp-popup-singlePage .cbp-l-project-title,
.wordpress-123shop .wpb_toggle,
.wordpress-123shop #content h4.wpb_toggle,
.wordpress-123shop .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header a,
.wpb_alert .messagebox_text,
.wordpress-123shop .wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav a,
.wordpress-123shop .wpb_content_element .wpb_accordion_header a,
.lpd-portfolio-item .title,
.widget-meta,
.featured-module-3-text,
.featured-module-1-content,
.mm1_description,
.mm2_description,
.mm3_description,
.mm4_title,
.mm5_title,
.new_module_content .description,
.tmc_info,
.vc_lpd_testiomonial .testiomonial_content,
.meta-block-content,
.iconitem .content,
.lpd-shopping-cart-list .empty,
.lpd-product-thumbnail .loop-shop-btn,
ul.lpd-products li.type-product h3,
.lpd-out-of-s,
.lpd-onsale,
.pn-btn,
.wordpress-456ecology .woocommerce ul.cart_list li a,
.wordpress-456ecology .woocommerce ul.product_list_widget li a,
.wordpress-456ecology.woocommerce-page ul.cart_list li a,
.wordpress-456ecology.woocommerce-page ul.product_list_widget li a,
.thumbnail-item-title,
.mh_description,
.cbp-l-caption-desc,
.cbp-l-grid-team-position,
.cbp-l-grid-projects-desc,
.cbp-l-grid-blog-comments,
.cbp-l-grid-blog-split,
.cbp-l-grid-blog-date,
.mi-slider nav a,
.mi-slider ul li h4,
.woocommerce-shop-assistant .assistant-phrase,
a.at_logo_carousel_dismiss_button,
.wordpress-456ecology .at_logo_carousel_calltoaction_block h2,
.wordpress-456ecology a.at_logo_carousel_button{
	font-family:
    <?php if($elements_font_family == 'Open+Sans'){
        echo "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Titillium+Web'){
        echo "'Titillium Web', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Oxygen'){
        echo "'Oxygen', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Quicksand'){
        echo "'Quicksand', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Lato'){
        echo "'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Raleway'){
        echo "'Raleway', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Source+Sans+Pro'){
        echo "'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Dosis'){
        echo "'Dosis', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Exo'){
        echo "'Exo', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Arvo'){
        echo "'Arvo', serif";
    }elseif($elements_font_family == 'Vollkorn'){
        echo "'Vollkorn', serif";
    }elseif($elements_font_family == 'Ubuntu'){
        echo "'Ubuntu', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'PT+Sans'){
        echo "'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'PT+Serif'){
        echo "'PT Serif', serif";
    }elseif($elements_font_family == 'Droid+Sans'){
        echo "'Droid Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Droid+Serif'){
        echo "'Droid Serif', serif";
    }elseif($elements_font_family == 'Cabin'){
        echo "'Cabin', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Lora'){
        echo "'Lora', serif";
    }elseif($elements_font_family == 'Oswald'){
        echo "'Oswald', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Varela+Round'){
        echo "'Varela Round', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Unna'){
        echo "'Unna', serif";
    }elseif($elements_font_family == 'Rokkitt'){
        echo "'Rokkitt', serif";
    }elseif($elements_font_family == 'Merriweather'){
        echo "'Merriweather', serif";
    }elseif($elements_font_family == 'Bitter'){
        echo "'Bitter', serif";
    }elseif($elements_font_family == 'Kreon'){
        echo "'Kreon', serif";
    }elseif($elements_font_family == 'Playfair+Display'){
        echo "'Playfair Display', serif";
    }elseif($elements_font_family == 'Roboto+Slab'){
        echo "'Roboto Slab', serif";
    }elseif($elements_font_family == 'Bree+Serif'){
        echo "'Bree Serif', serif";
    }elseif($elements_font_family == 'Libre+Baskerville'){
        echo "'Libre Baskerville', serif";
    }elseif($elements_font_family == 'Cantata+One'){
        echo "'Cantata One', serif";
    }elseif($elements_font_family == 'Alegreya'){
        echo "'Alegreya', serif";
    }elseif($elements_font_family == 'Noto+Serif'){
        echo "'Noto Serif', serif";
    }elseif($elements_font_family == 'EB+Garamond'){
        echo "'EB Garamond', serif";
    }elseif($elements_font_family == 'Noticia+Text'){
        echo "'Noticia Text', serif";
    }elseif($elements_font_family == 'Old+Standard+TT'){
        echo "'Old Standard TT', serif";
    }elseif($elements_font_family == 'Crimson+Text'){
        echo "'Crimson Text', serif";
    }elseif($elements_font_family == 'Josefin+Sans'){
        echo "'Josefin Sans','Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($elements_font_family == 'Ubuntu'){
        echo "'Ubuntu','Helvetica Neue', Helvetica, Arial, sans-serif";
    }else{
       echo $elements_font_family; 
    }?>;
}
body{
	font-family:
    <?php if($body_font_family == 'Open+Sans'){
        echo "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Titillium+Web'){
        echo "'Titillium Web', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Oxygen'){
        echo "'Oxygen', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Quicksand'){
        echo "'Quicksand', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Lato'){
        echo "'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Raleway'){
        echo "'Raleway', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Source+Sans+Pro'){
        echo "'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Dosis'){
        echo "'Dosis', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Exo'){
        echo "'Exo', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Arvo'){
        echo "'Arvo', serif";
    }elseif($body_font_family == 'Vollkorn'){
        echo "'Vollkorn', serif";
    }elseif($body_font_family == 'Ubuntu'){
        echo "'Ubuntu', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'PT+Sans'){
        echo "'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'PT+Serif'){
        echo "'PT Serif', serif";
    }elseif($body_font_family == 'Droid+Sans'){
        echo "'Droid Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Droid+Serif'){
        echo "'Droid Serif', serif";
    }elseif($body_font_family == 'Cabin'){
        echo "'Cabin', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Lora'){
        echo "'Lora', serif";
    }elseif($body_font_family == 'Oswald'){
        echo "'Oswald', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Varela+Round'){
        echo "'Varela Round', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Unna'){
        echo "'Unna', serif";
    }elseif($body_font_family == 'Rokkitt'){
        echo "'Rokkitt', serif";
    }elseif($body_font_family == 'Merriweather'){
        echo "'Merriweather', serif";
    }elseif($body_font_family == 'Bitter'){
        echo "'Bitter', serif";
    }elseif($body_font_family == 'Kreon'){
        echo "'Kreon', serif";
    }elseif($body_font_family == 'Playfair+Display'){
        echo "'Playfair Display', serif";
    }elseif($body_font_family == 'Roboto+Slab'){
        echo "'Roboto Slab', serif";
    }elseif($body_font_family == 'Bree+Serif'){
        echo "'Bree Serif', serif";
    }elseif($body_font_family == 'Libre+Baskerville'){
        echo "'Libre Baskerville', serif";
    }elseif($body_font_family == 'Cantata+One'){
        echo "'Cantata One', serif";
    }elseif($body_font_family == 'Alegreya'){
        echo "'Alegreya', serif";
    }elseif($body_font_family == 'Noto+Serif'){
        echo "'Noto Serif', serif";
    }elseif($body_font_family == 'EB+Garamond'){
        echo "'EB Garamond', serif";
    }elseif($body_font_family == 'Noticia+Text'){
        echo "'Noticia Text', serif";
    }elseif($body_font_family == 'Old+Standard+TT'){
        echo "'Old Standard TT', serif";
    }elseif($body_font_family == 'Crimson+Text'){
        echo "'Crimson Text', serif";
    }elseif($body_font_family == 'Josefin+Sans'){
        echo "'Josefin Sans','Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($body_font_family == 'Ubuntu'){
        echo "'Ubuntu','Helvetica Neue', Helvetica, Arial, sans-serif";
    }else{
       echo $body_font_family; 
    }?>;
}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6 {
	font-family:
    <?php if($heading_font_family == 'Open+Sans'){
        echo "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Titillium+Web'){
        echo "'Titillium Web', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Oxygen'){
        echo "'Oxygen', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Quicksand'){
        echo "'Quicksand', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Lato'){
        echo "'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Raleway'){
        echo "'Raleway', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Source+Sans+Pro'){
        echo "'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Dosis'){
        echo "'Dosis', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Exo'){
        echo "'Exo', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Arvo'){
        echo "'Arvo', serif";
    }elseif($heading_font_family == 'Vollkorn'){
        echo "'Vollkorn', serif";
    }elseif($heading_font_family == 'Ubuntu'){
        echo "'Ubuntu', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'PT+Sans'){
        echo "'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'PT+Serif'){
        echo "'PT Serif', serif";
    }elseif($heading_font_family == 'Droid+Sans'){
        echo "'Droid Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Droid+Serif'){
        echo "'Droid Serif', serif";
    }elseif($heading_font_family == 'Cabin'){
        echo "'Cabin', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Lora'){
        echo "'Lora', serif";
    }elseif($heading_font_family == 'Oswald'){
        echo "'Oswald', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Varela+Round'){
        echo "'Varela Round', 'Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Unna'){
        echo "'Unna', serif";
    }elseif($heading_font_family == 'Rokkitt'){
        echo "'Rokkitt', serif";
    }elseif($heading_font_family == 'Merriweather'){
        echo "'Merriweather', serif";
    }elseif($heading_font_family == 'Bitter'){
        echo "'Bitter', serif";
    }elseif($heading_font_family == 'Kreon'){
        echo "'Kreon', serif";
    }elseif($heading_font_family == 'Playfair+Display'){
        echo "'Playfair Display', serif";
    }elseif($heading_font_family == 'Roboto+Slab'){
        echo "'Roboto Slab', serif";
    }elseif($heading_font_family == 'Bree+Serif'){
        echo "'Bree Serif', serif";
    }elseif($heading_font_family == 'Libre+Baskerville'){
        echo "'Libre Baskerville', serif";
    }elseif($heading_font_family == 'Cantata+One'){
        echo "'Cantata One', serif";
    }elseif($heading_font_family == 'Alegreya'){
        echo "'Alegreya', serif";
    }elseif($heading_font_family == 'Noto+Serif'){
        echo "'Noto Serif', serif";
    }elseif($heading_font_family == 'EB+Garamond'){
        echo "'EB Garamond', serif";
    }elseif($heading_font_family == 'Noticia+Text'){
        echo "'Noticia Text', serif";
    }elseif($heading_font_family == 'Old+Standard+TT'){
        echo "'Old Standard TT', serif";
    }elseif($heading_font_family == 'Crimson+Text'){
        echo "'Crimson Text', serif";
    }elseif($heading_font_family == 'Josefin+Sans'){
        echo "'Josefin Sans','Helvetica Neue', Helvetica, Arial, sans-serif";
    }elseif($heading_font_family == 'Ubuntu'){
        echo "'Ubuntu','Helvetica Neue', Helvetica, Arial, sans-serif";
    }else{
       echo $heading_font_family; 
    }?>;
}
</style>

<?php }?>
<?php add_action( 'wp_head', 'lpd_fonts_styles' );?>