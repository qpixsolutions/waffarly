<?php function vc_colio_widget_func( $atts, $content = null ) {

	lpd_colio();
	
   extract( shortcode_atts( array(
		'colio_type' => '',
		'post_type' => '',
		'columns' => '',
		'caption' => '',
		'thumbnail' => '',
		'items' => '',
		'filter' => '',
		'category_filter' => '',
		'description' => '',
		'placement' => '',
		'expandduration' => '',
		'expandeasing' => '',
		'collapseduration' => '',
		'collapseeasing' => '',
		'scrollduration' => '',
		'scrolleasing' => '',
		'syncscroll' => '',
		'scrolloffset' => '',
		'contentfadein' => '',
		'contentfadeout' => '',
		'contentdelay' => '',
		'navigation' => '',      
      
   ), $atts ) );
   
   global $shortcode_atts;
   
   $shortcode_atts = array(
		'colio_type' => $colio_type,
		'placement' => $placement,
		'expandduration' => $expandduration,
		'expandeasing' => $expandeasing,
		'collapseduration' => $collapseduration,
		'collapseeasing' => $collapseeasing,
		'scrollduration' => $scrollduration,
		'scrolleasing' => $scrolleasing,
		'syncscroll' => $syncscroll,
		'scrolloffset' => $scrolloffset,
		'contentfadein' => $contentfadein,
		'contentfadeout' => $contentfadeout,
		'contentdelay' => $contentdelay,
		'navigation' => $navigation,
   );
   
if($columns=="2"){
	$columns = 'col-md-6';
} elseif($columns=="3"){
	$columns = 'col-md-4';
} elseif($columns=="6"){
	$columns = 'col-md-2';
} else{
	$columns = 'col-md-3';
}

$cat_terms="";

if($post_type=="portfolio"){
	$post_type="portfolio";
	$cat_terms="portfolio_category";
} elseif($post_type=="team"){
	$post_type="team";
	$cat_terms="team_category";
} elseif($post_type=="product"){
	$post_type="product";
	$cat_terms="product_cat";
} else{
	$post_type="post";
	$cat_terms="category";
}

if($thumbnail=="square") {
	$image_size = 'front-shop-thumb2';
} else if($thumbnail=="portrait") {
	$image_size = 'front-shop-thumb';
} else{
	$image_size = 'cubeportfolio';
}

if(!$items){
	$items='9999';
}

$thumbnail_filter = ot_get_option('thumbnail_filter');

global $wpdb, $woocommerce;

$category_filter_slugs = explode(",", $category_filter);
$category_filter_id = '';
foreach($category_filter_slugs as $category_filter_slug){
     $category_filter_id .= $wpdb->get_var("SELECT term_id FROM $wpdb->terms WHERE slug='$category_filter_slug'").',';
}

global $the_colio_t_ID;

$the_colio_t_ID = rand();	
   
   ob_start();?>
   
			<div class="lpd-colio clearfix">
				
				<?php if(!$filter){?>
				<div class="filters">
					<a href="#" class="filter-active"><?php _e('All', GETTEXT_DOMAIN) ?></a>
					<?php wp_list_categories(array('title_li' => '', 'style' => '','include' => $category_filter_id, 'taxonomy' => $cat_terms, 'walker' => new lpd_portfolio_walker_colio())); ?>
				</div><!-- filters -->
				<?php }?>
				
				<div class="clearfix"></div>
				
				<ul class="colio-list row colio-id-<?php echo $the_colio_t_ID;?>">
					
					<?php $i=0; $query = new WP_Query();?>
					<?php if($category_filter){?>
					    <?php $query->query('post_type='. $post_type .'&'. $cat_terms .'='. $category_filter .'&posts_per_page='. $items .'');?>
					<?php }else{?>
					    <?php $query->query('post_type='. $post_type .'&posts_per_page='. $items .'');?>
					<?php }?>
					<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>
					
					<?php $link = get_post_meta(get_the_ID(), 'link_post_meta', true); ?>
					<?php $terms = get_the_terms(get_the_ID(), $cat_terms ); ?>
						
						<li class="<?php if($columns) { echo $columns; } else{ echo 'col-md-3';}?><?php if($terms) : foreach ($terms as $term) { echo ' lpd_colio_'.$term->slug.''; } endif; ?>"  data-content="#colio_<?php the_ID();?>">
							<div class="thumb">
								<div class="view">
									<a class="button btn-primary btn colio-link" href="#"><?php _e('View More', GETTEXT_DOMAIN) ?></a>
								</div>
	                            <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) {?>
	                                <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $image_size ); echo $image[0];?>" alt="" width="100%">
	                            <?php } else{?>
                            		<img src="
	                                <?php if($thumbnail=="square") {?>
	                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-square.png'; ?>
	                                <?php } else if($thumbnail=="portrait") {?>
	                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-portrait.png'; ?>
									<?php } else{?>
	                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image.png'; ?>
	                                <?php }?>
									" alt="" width="100%">
	                            <?php }?>
	                            <?php if($thumbnail_filter){?>
	                            	<div class="white_isolated"></div>
	                            <?php }?>
							</div>
	                        <?php if($link){?>
		                        <h4><a class="colio-link" href="<?php echo $link;?>"><?php the_title();?></a></h4>
		                    <?php } else{?>
		                        <h4><a class="colio-link" href="<?php the_permalink();?>"><?php the_title();?></a></h4>
		                    <?php }?>
							<?php if($caption){?><p><?php echo lpd_excerpt_more($caption);?></p><?php }?>
						</li>
						
						
					<?php endwhile; endif; ?>
	
					
				</ul><!-- list -->

			</div><!-- portfolio -->
			
			
			<?php $query = new WP_Query();?>
			<?php if($category_filter){?>
			    <?php $query->query('post_type='. $post_type .'&'. $cat_terms .'='. $category_filter .'&posts_per_page='. $items .'');?>
			<?php }else{?>
			    <?php $query->query('post_type='. $post_type .'&posts_per_page='. $items .'');?>
			<?php }?>
			<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>
			
			<?php $link = get_post_meta(get_the_ID(), 'link_post_meta', true); ?>
			<?php $terms = get_the_terms(get_the_ID(), $cat_terms ); ?>
			<div id="colio_<?php the_ID();?>" class="colio-content">
			
				<?php if($colio_type=="gallery"){?>
				
					<div class="main">
					
						<h3<?php if($navigation=="false"){?> class="no-navigation"<?php }?>><?php the_title();?></h3>
						<?php if($post_type=="product"){ ?>
							<?php $catalog_type = ot_get_option('catalog_type');?>
							<?php if($catalog_type!="purchases_prices"){?>
								<div class="colio-product-price">
									<?php woocommerce_template_loop_price(); ?>
								</div>
							<?php }?>
						<?php }?>
						<?php if($description){?><p><?php echo lpd_excerpt_more($description);?></p><?php }?>
						<div class="divider10"></div>
						
					    <?php if($link){?>
	                        
	                        <a class="visit-link btn-primary btn" href="<?php echo $link;?>"><?php _e('More Info', GETTEXT_DOMAIN) ?></a>
	                    <?php } else{?>
	                        
	                        <a class="visit-link btn-primary btn" href="<?php the_permalink();?>"><?php _e('More Info', GETTEXT_DOMAIN) ?></a>
	                    <?php }?>
	
					</div><!-- main -->
					
					<div class="side">
						<ul class="feed">
						
							<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) {?>
	                        	<?php $alt = get_post_meta(get_post_thumbnail_id( get_the_ID() ), '_wp_attachment_image_alt', true);?>
	                            <li><a class="fancybox" href="<?php $image_lg = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "large" ); echo $image_lg[0];?>"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'front-shop-thumb2' ); echo $image[0];?>" alt="<?php echo $alt;?>" width="100%"></a></li>
	                        <?php } else{?>
	                        	<li><a class="fancybox" href="<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-square.png'; ?>"><img src="<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-square.png'; ?>" alt="<?php _e('add featured image', GETTEXT_DOMAIN) ?>" width="100%"></a></li>
	                        <?php }?>
	                        
							<?php if($post_type=="portfolio"){?>
								<?php $images = get_post_meta(get_the_ID(), 'vdw_gallery_id', true);?>
								<?php if(!$images): $images = array(); endif;?>
								<?php foreach ($images as $image) {?>
								<li><a class="fancybox" href="<?php $image_lq = wp_get_attachment_image_src( $image, "large" ); echo $image_lg[0];?>"><img alt="<?php echo $alt; ?>" src="<?php $image = wp_get_attachment_image_src( $image, 'front-shop-thumb2' ); echo $image[0];?>" width="100%"></a></li>
								<?php }?>						
							<?php } elseif($post_type=="team"){?>
							
							    <?php $args = array(
							    'numberposts' => 9999,
							    'offset' => 0,
							    'post_parent' => get_the_ID(),
							    'post_type' => 'attachment',
							    'nopaging' => false,
							    'post_mime_type' => 'image',
							    'order' => 'ASC',
							    'orderby' => 'menu_order ID',
							    'post_status' => 'any',
							    'exclude'     => get_post_thumbnail_id()
							    );
							    $attachments = get_children($args);?>
							                
							    <?php if ($attachments) {?>
							    
									<?php foreach($attachments as $attachment) {
								        $title = $attachment->post_title;
								        $image_lg = wp_get_attachment_image_src($attachment->ID, 'large', false);
								        $image = wp_get_attachment_image_src($attachment->ID, 'front-shop-thumb2', false);?> 
								        <li><a class="fancybox" href="<?php echo $image_lg[0] ?>"><img src="<?php echo $image[0] ?>" alt="<?php echo $title ?>" width="100%"></a></li>
									<?php }?>
							    
							    <?php }?>
	
							<?php } elseif($post_type=="product"){?>
							
								<?php global $product;?>
							
								<?php $attachment_ids = $product->get_gallery_attachment_ids();
								if($attachment_ids){
									$output ="";
									foreach ( $attachment_ids as $attachment_id ) {
										$alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
										$image_lg = wp_get_attachment_image_src($attachment_id, 'large', false);
										$output .= '<li><a class="fancybox" href="'.$image_lg[0].'"><img alt="'.$alt.'" src="';
										$image = wp_get_attachment_image_src( $attachment_id, 'front-shop-thumb2' );
										$output .= $image[0];
										$output .= '" width="100%"></a></li>';
									}
									
									echo $output;
								}?>
	
							<?php } else{?>
	
							    <?php $args = array(
							    'numberposts' => 9999,
							    'offset' => 0,
							    'post_parent' => get_the_ID(),
							    'post_type' => 'attachment',
							    'nopaging' => false,
							    'post_mime_type' => 'image',
							    'order' => 'ASC',
							    'orderby' => 'menu_order ID',
							    'post_status' => 'any',
							    'exclude'     => get_post_thumbnail_id()
							    );
							    $attachments = get_children($args);?>
							                
							    <?php if ($attachments) {?>
							    
									<?php foreach($attachments as $attachment) {
								        $title = $attachment->post_title;
								        $image = wp_get_attachment_image_src($attachment->ID, 'front-shop-thumb2', false);
								        $image_lg = wp_get_attachment_image_src($attachment->ID, 'large', false);?>  
								        <li><a class="fancybox" href="<?php echo $image_lg[0] ?>"><img src="<?php echo $image[0] ?>" alt="<?php echo $title ?>" width="100%"></a></li>
									<?php }?>
							    
							    <?php }?>
	
							<?php }?>

						</ul>
					</div><!-- side -->
				
				<?php } else{?>
			
					<div class="side">
	
					<div class="flexslider">
					
						<?php if($post_type=="product"){ ?>
						
							<?php woocommerce_show_product_loop_sale_flash(); ?>
							
							<?php global $product;?>
						
							<?php if ( ! $product->is_in_stock() ) : ?>
								<span class="lpd-out-of-s<?php if ($product->is_on_sale()) { ?> on-sale-products<?php }?>"><?php _e( 'Out of Stock', GETTEXT_DOMAIN ); ?></span>
							<?php endif; ?>
							
						<?php } ?>
					
						<ul class="slides">
							
	                        <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) {?>
	                        	<?php $alt = get_post_meta(get_post_thumbnail_id( get_the_ID() ), '_wp_attachment_image_alt', true);?>
	                            <li><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $image_size ); echo $image[0];?>" alt="<?php echo $alt;?>" width="100%"></li>
	                        <?php } else{?>
	                        	<li><img src="
                                <?php if($thumbnail=="square") {?>
                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-square.png'; ?>
                                <?php } else if($thumbnail=="portrait") {?>
                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image-portrait.png'; ?>
								<?php } else{?>
                                	<?php echo get_template_directory_uri(). '/assets/img/add-featured-image.png'; ?>
                                <?php }?>
	                        	" alt="<?php _e('add featured image', GETTEXT_DOMAIN) ?>" width="100%"></li>
	                        <?php }?>
	                        
							<?php if($post_type=="portfolio"){?>
								<?php $images = get_post_meta(get_the_ID(), 'vdw_gallery_id', true);?>
								<?php if(!$images): $images = array(); endif;?>
								<?php foreach ($images as $image) {?>
								<li><img alt="<?php echo $alt; ?>" src="<?php $image = wp_get_attachment_image_src( $image, $image_size ); echo $image[0];?>" width="100%"></li>
								<?php }?>						
							<?php } elseif($post_type=="team"){?>
							
							    <?php $args = array(
							    'numberposts' => 9999,
							    'offset' => 0,
							    'post_parent' => get_the_ID(),
							    'post_type' => 'attachment',
							    'nopaging' => false,
							    'post_mime_type' => 'image',
							    'order' => 'ASC',
							    'orderby' => 'menu_order ID',
							    'post_status' => 'any',
							    'exclude'     => get_post_thumbnail_id()
							    );
							    $attachments = get_children($args);?>
							                
							    <?php if ($attachments) {?>
							    
									<?php foreach($attachments as $attachment) {
								        $title = $attachment->post_title;
								        $image = wp_get_attachment_image_src($attachment->ID, $image_size, false);?> 
								        <li><img src="<?php echo $image[0] ?>" alt="<?php echo $title ?>" width="100%"></li>
									<?php }?>
							    
							    <?php }?>
	
							<?php } elseif($post_type=="product"){?>
							
								<?php global $product;?>
							
								<?php $attachment_ids = $product->get_gallery_attachment_ids();
								if($attachment_ids){
									$output ="";
									foreach ( $attachment_ids as $attachment_id ) {
										$image_link = wp_get_attachment_url( $attachment_id );
										$alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
										if ( ! $image_link )continue;
										$output .= '<li><img alt="'.$alt.'" src="';
										$image = wp_get_attachment_image_src( $attachment_id, $image_size );
										$output .= $image[0];
										$output .= '" width="100%"></li>';
									}
									
									echo $output;
								}?>
	
							<?php } else{?>
	
							    <?php $args = array(
							    'numberposts' => 9999,
							    'offset' => 0,
							    'post_parent' => get_the_ID(),
							    'post_type' => 'attachment',
							    'nopaging' => false,
							    'post_mime_type' => 'image',
							    'order' => 'ASC',
							    'orderby' => 'menu_order ID',
							    'post_status' => 'any',
							    'exclude'     => get_post_thumbnail_id()
							    );
							    $attachments = get_children($args);?>
							                
							    <?php if ($attachments) {?>
							    
									<?php foreach($attachments as $attachment) {
								        $title = $attachment->post_title;
								        $image = wp_get_attachment_image_src($attachment->ID, $image_size, false);?> 
								        <li><img src="<?php echo $image[0] ?>" alt="<?php echo $title ?>" width="100%"></li>
									<?php }?>
							    
							    <?php }?>
	
							<?php }?>
							
						</ul>
					</div>
					
					</div><!-- side -->
						
					<div class="main">
						
						<h3><?php the_title();?></h3>
						<?php if($post_type=="product"){ ?>
							<?php $catalog_type = ot_get_option('catalog_type');?>
							<?php if($catalog_type!="purchases_prices"){?>
								<div class="colio-product-price">
									<?php woocommerce_template_loop_price(); ?>
								</div>
							<?php }?>
						<?php }?>
						<?php if($description){?><p><?php echo lpd_excerpt_more($description);?></p><?php }?>
						<div class="divider10"></div>
						
					    <?php if($link){?>
	                        
	                        <a class="visit-link btn-primary btn" href="<?php echo $link;?>"><?php _e('More Info', GETTEXT_DOMAIN) ?></a>
	                    <?php } else{?>
	                        
	                        <a class="visit-link btn-primary btn" href="<?php the_permalink();?>"><?php _e('More Info', GETTEXT_DOMAIN) ?></a>
	                    <?php }?>
						
					</div><!-- main -->
					
				<?php }?>
								
			</div>
						
			<?php endwhile; endif; wp_reset_query();?>
			
	<?php 
	$colio_js = new colio_class();
	
	$colio_js->colio_callback();	
	?>
	
   <?php return ob_get_clean();
   
}
add_shortcode( 'vc_colio_widget', 'vc_colio_widget_func' );

	
class colio_class
{
    protected static $var = '';

    public static function colio_callback() 
    {
    
	global $the_colio_t_ID;

	global $shortcode_atts;
	
	$colio_type = $shortcode_atts['colio_type'];
	$placement = $shortcode_atts['placement'];
	$expandduration = $shortcode_atts['expandduration'];
	$expandeasing = $shortcode_atts['expandeasing'];
	$collapseduration = $shortcode_atts['collapseduration'];
	$collapseeasing = $shortcode_atts['collapseeasing'];
	$scrollduration = $shortcode_atts['scrollduration'];
	$scrolleasing = $shortcode_atts['scrolleasing'];
	$syncscroll = $shortcode_atts['syncscroll'];
	$scrolloffset = $shortcode_atts['scrolloffset'];
	$contentfadein = $shortcode_atts['contentfadein'];
	$contentfadeout = $shortcode_atts['contentfadeout'];
	$contentdelay = $shortcode_atts['contentdelay'];
	$navigation = $shortcode_atts['navigation'];
	
ob_start();?>

<script>

jQuery(document).ready(function(){
	// "colio" plugin
	var colio_run = function(){
		jQuery('#lpd-colio-expanded').remove();
		jQuery('.lpd-colio .colio-list.colio-id-<?php echo $the_colio_t_ID;?>').colio({
			id: 'lpd-colio-expanded',
			<?php if($colio_type=="slider"){?>
			theme: 'slider',
			<?php } else{?>
		    theme: 'gallery',
		    <?php }?>
		    placement: '<?php echo $placement;?>',                // viewport placement - "before", "after", "inside" or "#id"  
		    expandLink: '.colio-link',          // selector for element to expand colio viewport    
		    expandDuration: <?php echo $expandduration;?>,                // duration of expand animation, ms  
		    expandEasing: '<?php echo $expandeasing;?>',              // easing for expand animation  
		    collapseDuration: <?php echo $collapseduration;?>,              // duration of collapse animation, ms  
		    collapseEasing: '<?php echo $collapseeasing;?>',            // easing for collapse animation  
		    scrollDuration: <?php echo $scrollduration;?>,                // page scroll duration, ms  
		    scrollEasing: '<?php echo $scrolleasing;?>',              // page scroll easing  
		    syncScroll: <?php echo $syncscroll;?>,                  // sync page scroll with expand/collapse of colio viewport  
		    scrollOffset: <?php echo $scrolloffset;?>,                   // page offset when colio viewport is expanded  
		    contentFadeIn: <?php echo $contentfadein;?>,                 // content fade-in duration, ms  
		    contentFadeOut: <?php echo $contentfadeout;?>,                // content fade-out duration, ms  
		    contentDelay: <?php echo $contentdelay;?>,                  // content fade-in delay on expand, ms  
		    navigation: <?php echo $navigation;?>,                   // enable or disable next/previous navigation  
		    closeText: '<span><?php _e("Close", GETTEXT_DOMAIN);?></span>',    // text/html for close button  
		    nextText: '<span><?php _e("Next", GETTEXT_DOMAIN);?></span>',      // text/html for next button  
		    prevText: '<span><?php _e("Prev", GETTEXT_DOMAIN);?></span>',      // text/html for previous button  
		    contentFilter: '',                  // selector to filter content  
		    hiddenItems: '',             // selector to exclude hidden portfolio items   
			
			<?php if($colio_type=="slider"){?>
			onContent: function(content){
				// init "flexslider" plugin
				jQuery('.flexslider', content).flexslider({slideshow: false, animationSpeed: 300});
			}
			<?php } else{?>
			onContent: function(content){
				// init "fancybox" plugin
				jQuery('.fancybox', content).fancybox();
			}
			<?php }?>
		});
	};
	
	colio_run();
				
	// "quicksand" plugin
	var quicksand_run = function(items){
		jQuery('.lpd-colio .colio-list').quicksand(items, {
			retainExisting: false, adjustWidth:false, duration: 500
		}, colio_run);
	};
	
	jQuery('.lpd-colio .colio-list li').each(function(n){
		jQuery(this).attr('data-id', n);
	});
	
	var copy = jQuery('.lpd-colio .colio-list li').clone();
	
	jQuery('.lpd-colio .filters a').click(function(){
		jQuery(this).addClass('filter-active').siblings().removeClass('filter-active');
		var href = jQuery(this).attr('href').substr(1);
		filter = href ? '.' + href : '*';
		quicksand_run(copy.filter(filter));
		return false;
	});
	
});
       
</script>

<?php $script = ob_get_clean();

        self::$var[] = $script;

        add_action( 'wp_footer', array ( __CLASS__, 'footer' ), 20 );         
    }

	public static function footer() 
	{
	    foreach( self::$var as $script ){
	        echo $script;
	    }
	}

}

vc_map(array(
   "name" => __("Colio Widget", GETTEXT_DOMAIN),
   "base" => "vc_colio_widget",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
   
		array(
			"type" => "dropdown",
			"heading" => __("Widget Type", GETTEXT_DOMAIN),
			"param_name" => "colio_type",
			"value" => array(__('slider', GETTEXT_DOMAIN) => "slider", __('gallery', GETTEXT_DOMAIN) => "gallery"),
			"description" => __("Select widget type.", GETTEXT_DOMAIN)
		),
		array(
			"type" => "dropdown",
			"heading" => __("Post Type", GETTEXT_DOMAIN),
			"param_name" => "post_type",
			"value" => array(__('Blog Posts', GETTEXT_DOMAIN) => "post", __('Portfolio Posts', GETTEXT_DOMAIN) => "portfolio", __('Product Posts', GETTEXT_DOMAIN) => "product", __('Team Posts', GETTEXT_DOMAIN) => "team"),
			"description" => __("Select post type.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("Columns", GETTEXT_DOMAIN),
			"param_name" => "columns",
			"value" => array('2' => "2", '3' => "3", '4' => "4", '6' => "6"),
			"description" => __("Select number of columns.", GETTEXT_DOMAIN)
	    ),
		array(
			"type" => "dropdown",
			"heading" => __("Caption", GETTEXT_DOMAIN),
			"param_name" => "caption",
			"value" => array(__('none', GETTEXT_DOMAIN) => "", __('10 words', GETTEXT_DOMAIN) => "10", __('20 words', GETTEXT_DOMAIN) => "20", __('30 words', GETTEXT_DOMAIN) => "30", __('40 words', GETTEXT_DOMAIN) => "40"),
			"description" => __("Select caption type.", GETTEXT_DOMAIN)
		),
		array(
			"type" => "dropdown",
			"heading" => __("Thumbnail Style", GETTEXT_DOMAIN),
			"param_name" => "thumbnail",
			"value" => array(__('Landscape', GETTEXT_DOMAIN) => "landscape", __('Portrait', GETTEXT_DOMAIN) => "portrait", __('Square', GETTEXT_DOMAIN) => "square"),
			"description" => __("Select post type.", GETTEXT_DOMAIN)
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Items", GETTEXT_DOMAIN),
			 "param_name" => "items",
			 "value" => '',
			 "description" => __("The number of items to load.", GETTEXT_DOMAIN)
		),
		array(
	      "type" => 'checkbox',
	      "heading" => __("Filter", GETTEXT_DOMAIN),
	      "param_name" => "filter",
	      "description" => __("Check to hide colio filter.", GETTEXT_DOMAIN),
	      "value" => Array(__("disable", GETTEXT_DOMAIN) => 'disable')
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Category Filter", GETTEXT_DOMAIN),
			 "param_name" => "category_filter",
			 "value" => '',
			 "description" => __("Enter categories [slugs separated by commas] you would like to display, value for example 'cat1,cat2,cat3'.", GETTEXT_DOMAIN)
		),
		array(
			"type" => "dropdown",
			"heading" => __("Description", GETTEXT_DOMAIN),
			"param_name" => "description",
			"value" => array(__('100 words', GETTEXT_DOMAIN) => "100", __('120 words', GETTEXT_DOMAIN) => "120", __('130 words', GETTEXT_DOMAIN) => "130", __('140 words', GETTEXT_DOMAIN) => "140",__('150 words', GETTEXT_DOMAIN) => "150",__('160 words', GETTEXT_DOMAIN) => "160",__('170 words', GETTEXT_DOMAIN) => "170",__('180 words', GETTEXT_DOMAIN) => "180",__('190 words', GETTEXT_DOMAIN) => "190",__('200 words', GETTEXT_DOMAIN) => "200",__('210 words', GETTEXT_DOMAIN) => "210",__('220 words', GETTEXT_DOMAIN) => "220",__('230 words', GETTEXT_DOMAIN) => "230",__('240 words', GETTEXT_DOMAIN) => "240",__('250 words', GETTEXT_DOMAIN) => "250"),
			"description" => __("Select caption type.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("Placement", GETTEXT_DOMAIN),
			"param_name" => "placement",
			"value" => array(__('before', GETTEXT_DOMAIN) => "before", __('after', GETTEXT_DOMAIN) => "after", __('inside', GETTEXT_DOMAIN) => "inside"),
			"description" => __("Select placement position.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("expandDuration", GETTEXT_DOMAIN),
			 "param_name" => "expandduration",
			 "value" => '500',
			 "description" => __("Duration of expand animation, ms.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("expandEasing", GETTEXT_DOMAIN),
			"param_name" => "expandeasing",
			"value" => array('linear' => "linear", 'swing' => "swing", 'easeInQuad' => "easeInQuad", 'easeOutQuad' => "easeOutQuad", 'easeInOutQuad' => "easeInOutQuad", 'easeInCubic' => "easeInCubic", 'easeOutCubic' => "easeOutCubic", 'easeInOutCubic' => "easeInOutCubic", 'easeInQuart' => "easeInQuart", 'easeOutQuart' => "easeOutQuart", 'easeInOutQuart' => "easeInOutQuart", 'easeInQuint' => "easeInQuint", 'easeOutQuint' => "easeOutQuint", 'easeInOutQuint' => "easeInOutQuint", 'easeInExpo' => "easeInExpo", 'easeOutExpo' => "easeOutExpo", 'easeInOutExpo' => "easeInOutExpo", 'easeInSine' => "easeInSine", 'easeOutSine' => "easeOutSine", 'easeInOutSine' => "easeInOutSine", 'easeInCirc' => "easeInCirc", 'easeOutCirc' => "easeOutCirc", 'easeInOutCirc' => "easeInOutCirc", 'easeInElastic' => "easeInElastic", 'easeOutElastic' => "easeOutElastic", 'easeInOutElastic' => "easeInOutElastic", 'easeInBack' => "easeInBack", 'easeOutBack' => "easeOutBack", 'easeInOutBack' => "easeInOutBack", 'easeInBounce' => "easeInBounce", 'easeOutBounce' => "easeOutBounce", 'seaseInOutBounce' => "easeInOutBounce"),
			"description" => __("Easing for expand animation.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("collapseDuration", GETTEXT_DOMAIN),
			 "param_name" => "collapseduration",
			 "value" => '300',
			 "description" => __("Duration of collapse animation, ms.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("collapseEasing", GETTEXT_DOMAIN),
			"param_name" => "collapseeasing",
			"value" => array('linear' => "linear", 'swing' => "swing", 'easeInQuad' => "easeInQuad", 'easeOutQuad' => "easeOutQuad", 'easeInOutQuad' => "easeInOutQuad", 'easeInCubic' => "easeInCubic", 'easeOutCubic' => "easeOutCubic", 'easeInOutCubic' => "easeInOutCubic", 'easeInQuart' => "easeInQuart", 'easeOutQuart' => "easeOutQuart", 'easeInOutQuart' => "easeInOutQuart", 'easeInQuint' => "easeInQuint", 'easeOutQuint' => "easeOutQuint", 'easeInOutQuint' => "easeInOutQuint", 'easeInExpo' => "easeInExpo", 'easeOutExpo' => "easeOutExpo", 'easeInOutExpo' => "easeInOutExpo", 'easeInSine' => "easeInSine", 'easeOutSine' => "easeOutSine", 'easeInOutSine' => "easeInOutSine", 'easeInCirc' => "easeInCirc", 'easeOutCirc' => "easeOutCirc", 'easeInOutCirc' => "easeInOutCirc", 'easeInElastic' => "easeInElastic", 'easeOutElastic' => "easeOutElastic", 'easeInOutElastic' => "easeInOutElastic", 'easeInBack' => "easeInBack", 'easeOutBack' => "easeOutBack", 'easeInOutBack' => "easeInOutBack", 'easeInBounce' => "easeInBounce", 'easeOutBounce' => "easeOutBounce", 'seaseInOutBounce' => "easeInOutBounce"),
			"description" => __("Easing for collapse animation.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("scrollDuration", GETTEXT_DOMAIN),
			 "param_name" => "scrollduration",
			 "value" => '300',
			 "description" => __("Duration of scroll animation, ms.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("scrollEasing", GETTEXT_DOMAIN),
			"param_name" => "scrolleasing",
			"value" => array('linear' => "linear", 'swing' => "swing", 'easeInQuad' => "easeInQuad", 'easeOutQuad' => "easeOutQuad", 'easeInOutQuad' => "easeInOutQuad", 'easeInCubic' => "easeInCubic", 'easeOutCubic' => "easeOutCubic", 'easeInOutCubic' => "easeInOutCubic", 'easeInQuart' => "easeInQuart", 'easeOutQuart' => "easeOutQuart", 'easeInOutQuart' => "easeInOutQuart", 'easeInQuint' => "easeInQuint", 'easeOutQuint' => "easeOutQuint", 'easeInOutQuint' => "easeInOutQuint", 'easeInExpo' => "easeInExpo", 'easeOutExpo' => "easeOutExpo", 'easeInOutExpo' => "easeInOutExpo", 'easeInSine' => "easeInSine", 'easeOutSine' => "easeOutSine", 'easeInOutSine' => "easeInOutSine", 'easeInCirc' => "easeInCirc", 'easeOutCirc' => "easeOutCirc", 'easeInOutCirc' => "easeInOutCirc", 'easeInElastic' => "easeInElastic", 'easeOutElastic' => "easeOutElastic", 'easeInOutElastic' => "easeInOutElastic", 'easeInBack' => "easeInBack", 'easeOutBack' => "easeOutBack", 'easeInOutBack' => "easeInOutBack", 'easeInBounce' => "easeInBounce", 'easeOutBounce' => "easeOutBounce", 'seaseInOutBounce' => "easeInOutBounce"),
			"description" => __("Easing for scroll animation.", GETTEXT_DOMAIN)
	    ),
	    array(
			"type" => "dropdown",
			"heading" => __("syncScroll", GETTEXT_DOMAIN),
			"param_name" => "syncscroll",
			"value" => array('false' => "false", 'true' => "true"),
			"description" => __("Sync page scroll with expand/collapse of colio viewport.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("scrollOffset", GETTEXT_DOMAIN),
			 "param_name" => "scrolloffset",
			 "value" => '10',
			 "description" => __("Page offset when colio viewport is expanded.", GETTEXT_DOMAIN)
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("contentFadeIn", GETTEXT_DOMAIN),
			 "param_name" => "contentfadein",
			 "value" => '500',
			 "description" => __("Content fade-in duration, ms.", GETTEXT_DOMAIN)
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("contentFadeOut", GETTEXT_DOMAIN),
			 "param_name" => "contentfadeout",
			 "value" => '200',
			 "description" => __("Content fade-out duration, ms.", GETTEXT_DOMAIN)
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("contentDelay", GETTEXT_DOMAIN),
			 "param_name" => "contentdelay",
			 "value" => '200',
			 "description" => __("Content fade-in delay on expand, ms.", GETTEXT_DOMAIN)
		),
	    array(
			"type" => "dropdown",
			"heading" => __("Navigation", GETTEXT_DOMAIN),
			"param_name" => "navigation",
			"value" => array('false' => "false", 'true' => "true"),
			"description" => __("Enable or disable next/previous navigation.", GETTEXT_DOMAIN)
	    ),
   )
));

?>