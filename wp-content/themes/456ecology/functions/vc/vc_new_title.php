<?php

function lpd_title_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'title' => '',
      'icon' => '',
      'custom_icon' => '',
   ), $atts ) );
 
   #$content = wpb_js_remove_wpautop($content); // fix unclosed/unwanted paragraph tags in $content
   
   if($custom_icon){
   
   $custom_icon = wp_get_attachment_image_src( $custom_icon, 'full' );
   
   $icon = '<h4 style="background-image:url('.$custom_icon[0].')">';
   
   } else{
   
	   if($icon){
		   $icon = '<h4 class="'.$icon.'">';
	   }
   
   }
   
    return '<div class="lpd_title">'.$icon.''.$title.'</h4></div>';		
    
}
add_shortcode( 'vc_lpd_title', 'lpd_title_func' );

vc_map(array(
   "name" => __("LPD Title", GETTEXT_DOMAIN),
   "base" => "vc_lpd_title",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => __("Lorem ipsum dolor", GETTEXT_DOMAIN),
			 "description" => __("Enter yout title.", GETTEXT_DOMAIN)
		),
		array(
			"type" => "dropdown",
			"heading" => __("Icon", GETTEXT_DOMAIN),
			"param_name" => "icon",
			"value" => array(		
__('leaf nature', GETTEXT_DOMAIN) => "leaf_nature",
__('paper plane', GETTEXT_DOMAIN) => "paper_plane",
__('leaf plant', GETTEXT_DOMAIN) => "leaf_plant",
__('light bulb', GETTEXT_DOMAIN) => "light_bulb_on",
__('delivery van', GETTEXT_DOMAIN) => "delivery_van",
__('shopping bag', GETTEXT_DOMAIN) => "shopping_bag",
			),
			"description" => __("Select icon.", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Custom Icon", GETTEXT_DOMAIN),
	      "param_name" => "custom_icon",
	      "value" => "",
	      "description" => __("Select custom icon from media library, recommended dimensions 32x32px.", GETTEXT_DOMAIN)
	    ),
   )
));


?>