<?php

function vc_lpd_products_func( $atts, $content = null ) {
   extract( shortcode_atts( array(
   		'title' => '',
      'ids' => '',
      'image' => '',
      'dark_bg' => '',      
   ), $atts ) );
   
   $image = wp_get_attachment_image_src( $image, 'full' );
 
	$out = '';
	$out .= '<div ';
	
	if($image){
		$out .= 'style="background-image: url('.$image[0].');"';
	}
	
	$out .= ' class="parallax-object ';
	
	if(!$image){
		$out .= 'no-parallax-img ';
	}
	
	$out .= 'parallax-product parallax-full';
	
	if($image){
	
		if($dark_bg){
			$out .= ' dark-plx-bg';
		} else{
			$out .= ' light-plx-bg';
		}
		
	} else{
	
		$out .= ' light-plx-bg';
	
	}
	
	if($image){
		$out .= '" data-stellar-background-ratio="0.5">';
	} else{
		$out .= '">';
	}
	
	$out .= '<div class="container">';
	$out .= '<div class="vc-lpd-woocommerce">';
	
	if($title){
		$out .= '<h2>'.$title.'</h2>';
	} else{
		$out .= '<div class="divider20"></div><div class="divider10"></div>';
	}
	
	$out .= do_shortcode('[products ids="'.$ids.'"]');
	$out .= '</div></div></div>';
	
    return $out;
}
add_shortcode( 'vc_lpd_products', 'vc_lpd_products_func' );

function vc_lpd_product_categories_func( $atts, $content = null ) {
   extract( shortcode_atts( array(
   		'title' => '',
      'number' => '',
      'orderby' => '',
      'order' => '',
      'columns' => '',
      'hide_empty' => '',
      'image' => '',
      'dark_bg' => '',      
   ), $atts ) );
   
   $image = wp_get_attachment_image_src( $image, 'full' );
 
	$out = '';
	$out .= '<div ';
	
	if($image){
		$out .= 'style="background-image: url('.$image[0].');"';
	}
	
	$out .= ' class="parallax-object ';
	
	if(!$image){
		$out .= 'no-parallax-img ';
	}
	
	$out .= 'parallax-product parallax-full';
	
	if($image){
	
		if($dark_bg){
			$out .= ' dark-plx-bg';
		} else{
			$out .= ' light-plx-bg';
		}
		
	} else{
	
		$out .= ' light-plx-bg';
	
	}
	
	if($image){
		$out .= '" data-stellar-background-ratio="0.5">';
	} else{
		$out .= '">';
	}
	
	$out .= '<div class="container">';
	$out .= '<div class="vc-lpd-woocommerce">';
	
	if($title){
		$out .= '<h2>'.$title.'</h2>';
	} else{
		$out .= '<div class="divider20"></div><div class="divider10"></div>';
	}
	
	$out .= do_shortcode('[product_categories number="'.$number.'" orderby="'.$orderby.'" order="'.$order.'" columns="'.$columns.'" hide_empty="'.$hide_empty.'"]');
	
	if($title){
		$out .= '<div class="divider20">';
	} else{
		$out .= '<div class="divider10">';
	}
	
	$out .= '</div></div></div></div>';
	
    return $out;
}
add_shortcode( 'vc_lpd_product_categories', 'vc_lpd_product_categories_func' );

function vc_lpd_product_category_func( $atts, $content = null ) {
   extract( shortcode_atts( array(
   		'title' => '',
      'per_page' => '',
      'columns' => '',
      'orderby' => '',
      'order' => '',
      'category' => '',
      'image' => '',
      'dark_bg' => '',      
   ), $atts ) );
   
   $image = wp_get_attachment_image_src( $image, 'full' );
 
	$out = '';
	$out .= '<div ';
	
	if($image){
		$out .= 'style="background-image: url('.$image[0].');"';
	}
	
	$out .= ' class="parallax-object ';
	
	if(!$image){
		$out .= 'no-parallax-img ';
	}
	
	$out .= 'parallax-product parallax-full';
	
	if($image){
	
		if($dark_bg){
			$out .= ' dark-plx-bg';
		} else{
			$out .= ' light-plx-bg';
		}
		
	} else{
	
		$out .= ' light-plx-bg';
	
	}
	
	if($image){
		$out .= '" data-stellar-background-ratio="0.5">';
	} else{
		$out .= '">';
	}
	
	$out .= '<div class="container">';
	$out .= '<div class="vc-lpd-woocommerce">';
	
	if($title){
		$out .= '<h2>'.$title.'</h2>';
	} else{
		$out .= '<div class="divider20"></div><div class="divider10"></div>';
	}
	
	$out .= do_shortcode('[product_category per_page="2" columns="2" orderby="date" order="DESC" category="prod_cat"]');
	$out .= '</div></div></div>';
	
    return $out;
}
add_shortcode( 'vc_lpd_product_category', 'vc_lpd_product_category_func' );


function vc_lpd_recent_products_func( $atts, $content = null ) {
   extract( shortcode_atts( array(
   		'title' => '',
      'per_page' => '',
      'columns' => '',
      'orderby' => '',
      'order' => '',
      'image' => '',
      'dark_bg' => '',      
   ), $atts ) );
   
   $image = wp_get_attachment_image_src( $image, 'full' );
 
	$out = '';
	$out .= '<div ';
	
	if($image){
		$out .= 'style="background-image: url('.$image[0].');"';
	}
	
	$out .= ' class="parallax-object ';
	
	if(!$image){
		$out .= 'no-parallax-img ';
	}
	
	$out .= 'parallax-product parallax-full';
	
	if($image){
	
		if($dark_bg){
			$out .= ' dark-plx-bg';
		} else{
			$out .= ' light-plx-bg';
		}
		
	} else{
	
		$out .= ' light-plx-bg';
	
	}
	
	if($image){
		$out .= '" data-stellar-background-ratio="0.5">';
	} else{
		$out .= '">';
	}
	
	$out .= '<div class="container">';
	$out .= '<div class="vc-lpd-woocommerce">';
	
	if($title){
		$out .= '<h2>'.$title.'</h2>';
	} else{
		$out .= '<div class="divider20"></div><div class="divider10"></div>';
	}
	
	$out .= do_shortcode('[recent_products per_page="'.$per_page.'" columns="'.$columns.'" orderby="'.$orderby.'" order="'.$order.'"]');
	$out .= '</div></div></div>';
	
    return $out;
}
add_shortcode( 'vc_lpd_recent_products', 'vc_lpd_recent_products_func' );

function vc_lpd_featured_products_func( $atts, $content = null ) {
   extract( shortcode_atts( array(
   		'title' => '',
      'per_page' => '',
      'columns' => '',
      'orderby' => '',
      'order' => '',
      'image' => '',
      'dark_bg' => '',      
   ), $atts ) );
   
   $image = wp_get_attachment_image_src( $image, 'full' );
 
	$out = '';
	$out .= '<div ';
	
	if($image){
		$out .= 'style="background-image: url('.$image[0].');"';
	}
	
	$out .= ' class="parallax-object ';
	
	if(!$image){
		$out .= 'no-parallax-img ';
	}
	
	$out .= 'parallax-product parallax-full';
	
	if($image){
	
		if($dark_bg){
			$out .= ' dark-plx-bg';
		} else{
			$out .= ' light-plx-bg';
		}
		
	} else{
	
		$out .= ' light-plx-bg';
	
	}
	
	if($image){
		$out .= '" data-stellar-background-ratio="0.5">';
	} else{
		$out .= '">';
	}
	
	$out .= '<div class="container">';
	$out .= '<div class="vc-lpd-woocommerce">';
	
	if($title){
		$out .= '<h2>'.$title.'</h2>';
	} else{
		$out .= '<div class="divider20"></div><div class="divider10"></div>';
	}
	
	$out .= do_shortcode('[featured_products per_page="2" columns="4" orderby="date" order="DESC"]');
	$out .= '</div></div></div>';
	
    return $out;
}
add_shortcode( 'vc_lpd_featured_products', 'vc_lpd_featured_products_func' );


vc_map(array(
   "name" => __("LPD Products", GETTEXT_DOMAIN),
   "base" => "vc_lpd_products",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => "",
			 "description" => "",
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Product IDs", GETTEXT_DOMAIN),
			 "param_name" => "ids",
			 "value" => "",
			 "description" => __("Enter comma-separated product ids.", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Dark Background", GETTEXT_DOMAIN),
      "param_name" => "dark_bg",
      "description" => __("Check if you are using dark background.", GETTEXT_DOMAIN),
      "value" => Array(__("Dark Background", GETTEXT_DOMAIN) => 'dark_bg')
    ),
   )
));

vc_map(array(
   "name" => __("LPD Product Categories", GETTEXT_DOMAIN),
   "base" => "vc_lpd_product_categories",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => "",
			 "description" => "",
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Number", GETTEXT_DOMAIN),
			 "param_name" => "number",
			 "value" => "",
			 "description" => __("Enter the number of products", GETTEXT_DOMAIN)
		),
    array(
      "type" => "dropdown",
      "heading" => __("Order by", GETTEXT_DOMAIN),
      "param_name" => "orderby",
      "value" => array( __("Date", GETTEXT_DOMAIN) => "date", __("ID", GETTEXT_DOMAIN) => "ID", __("Name", GETTEXT_DOMAIN) => "name" ),
      "description" => sprintf(__('Select how to sort product categories. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order way", GETTEXT_DOMAIN),
      "param_name" => "order",
      "value" => array( __("Descending", GETTEXT_DOMAIN) => "DESC", __("Ascending", GETTEXT_DOMAIN) => "ASC" ),
      "description" => sprintf(__('Designates the ascending or descending order. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Columns", GETTEXT_DOMAIN),
      "param_name" => "columns",
      "value" => array( '2' => "2", '3' => "3", '4' => "4" )
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Hide Empty", GETTEXT_DOMAIN),
      "param_name" => "hide_empty",
      "value" => array( __("Yes", GETTEXT_DOMAIN) => "1", __("No", GETTEXT_DOMAIN) => "0" ),
    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("IDs", GETTEXT_DOMAIN),
			 "param_name" => "ids",
			 "value" => "",
			 "description" => __("Enter comma-separated category ids you want to display", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Dark Background", GETTEXT_DOMAIN),
      "param_name" => "dark_bg",
      "description" => __("Check if you are using dark background.", GETTEXT_DOMAIN),
      "value" => Array(__("Dark Background", GETTEXT_DOMAIN) => 'dark_bg')
    ),
   )
));

vc_map(array(
   "name" => __("LPD Product Category ", GETTEXT_DOMAIN),
   "base" => "vc_lpd_product_category",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => "",
			 "description" => "",
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Per Page", GETTEXT_DOMAIN),
			 "param_name" => "per_page",
			 "value" => "",
			 "description" => __("Enter number of products you want to display.", GETTEXT_DOMAIN)
		),
    array(
      "type" => "dropdown",
      "heading" => __("Columns", GETTEXT_DOMAIN),
      "param_name" => "columns",
      "value" => array( '2' => "2", '3' => "3", '4' => "4" )
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order by", GETTEXT_DOMAIN),
      "param_name" => "orderby",
      "value" => array( __("Date", GETTEXT_DOMAIN) => "date", __("ID", GETTEXT_DOMAIN) => "ID", __("Author", GETTEXT_DOMAIN) => "author", __("Title", GETTEXT_DOMAIN) => "title", __("Name", GETTEXT_DOMAIN) => "name", __("Modified", GETTEXT_DOMAIN) => "modified", __("Random", GETTEXT_DOMAIN) => "rand", __("Comment count", GETTEXT_DOMAIN) => "comment_count", __("Menu order", GETTEXT_DOMAIN) => "menu_order" ),
      "description" => sprintf(__('Select how to sort product categories. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order way", GETTEXT_DOMAIN),
      "param_name" => "order",
      "value" => array( __("Descending", GETTEXT_DOMAIN) => "DESC", __("Ascending", GETTEXT_DOMAIN) => "ASC" ),
      "description" => sprintf(__('Designates the ascending or descending order. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Caterory", GETTEXT_DOMAIN),
			 "param_name" => "category",
			 "value" => "",
			 "description" => __("Enter comma-separated category slugs to show multiple products", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Dark Background", GETTEXT_DOMAIN),
      "param_name" => "dark_bg",
      "description" => __("Check if you are using dark background.", GETTEXT_DOMAIN),
      "value" => Array(__("Dark Background", GETTEXT_DOMAIN) => 'dark_bg')
    ),
    
   )
));


vc_map(array(
   "name" => __("LPD Recent Prodcuts", GETTEXT_DOMAIN),
   "base" => "vc_lpd_recent_products",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => "",
			 "description" => "",
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Per Page", GETTEXT_DOMAIN),
			 "param_name" => "per_page",
			 "value" => "",
			 "description" => __("Enter number of products you want to display.", GETTEXT_DOMAIN)
		),
    array(
      "type" => "dropdown",
      "heading" => __("Columns", GETTEXT_DOMAIN),
      "param_name" => "columns",
      "value" => array( '2' => "2", '3' => "3", '4' => "4" )
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order by", GETTEXT_DOMAIN),
      "param_name" => "orderby",
      "value" => array( __("Date", GETTEXT_DOMAIN) => "date", __("ID", GETTEXT_DOMAIN) => "ID", __("Author", GETTEXT_DOMAIN) => "author", __("Title", GETTEXT_DOMAIN) => "title", __("Name", GETTEXT_DOMAIN) => "name", __("Modified", GETTEXT_DOMAIN) => "modified", __("Random", GETTEXT_DOMAIN) => "rand", __("Comment count", GETTEXT_DOMAIN) => "comment_count", __("Menu order", GETTEXT_DOMAIN) => "menu_order" ),
      "description" => sprintf(__('Select how to sort product categories. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order way", GETTEXT_DOMAIN),
      "param_name" => "order",
      "value" => array( __("Descending", GETTEXT_DOMAIN) => "DESC", __("Ascending", GETTEXT_DOMAIN) => "ASC" ),
      "description" => sprintf(__('Designates the ascending or descending order. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Dark Background", GETTEXT_DOMAIN),
      "param_name" => "dark_bg",
      "description" => __("Check if you are using dark background.", GETTEXT_DOMAIN),
      "value" => Array(__("Dark Background", GETTEXT_DOMAIN) => 'dark_bg')
    ),
    
   )
));

vc_map(array(
   "name" => __("LPD Featured Products", GETTEXT_DOMAIN),
   "base" => "vc_lpd_featured_products",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
   
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => "",
			 "description" => "",
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Per Page", GETTEXT_DOMAIN),
			 "param_name" => "per_page",
			 "value" => "",
			 "description" => __("Enter number of products you want to display.", GETTEXT_DOMAIN)
		),
    array(
      "type" => "dropdown",
      "heading" => __("Columns", GETTEXT_DOMAIN),
      "param_name" => "columns",
      "value" => array( '2' => "2", '3' => "3", '4' => "4" )
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order by", GETTEXT_DOMAIN),
      "param_name" => "orderby",
      "value" => array( __("Date", GETTEXT_DOMAIN) => "date", __("ID", GETTEXT_DOMAIN) => "ID", __("Author", GETTEXT_DOMAIN) => "author", __("Title", GETTEXT_DOMAIN) => "title", __("Name", GETTEXT_DOMAIN) => "name", __("Modified", GETTEXT_DOMAIN) => "modified", __("Random", GETTEXT_DOMAIN) => "rand", __("Comment count", GETTEXT_DOMAIN) => "comment_count", __("Menu order", GETTEXT_DOMAIN) => "menu_order" ),
      "description" => sprintf(__('Select how to sort product categories. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Order way", GETTEXT_DOMAIN),
      "param_name" => "order",
      "value" => array( __("Descending", GETTEXT_DOMAIN) => "DESC", __("Ascending", GETTEXT_DOMAIN) => "ASC" ),
      "description" => sprintf(__('Designates the ascending or descending order. More at %s.', GETTEXT_DOMAIN), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>')
    ),
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Dark Background", GETTEXT_DOMAIN),
      "param_name" => "dark_bg",
      "description" => __("Check if you are using dark background.", GETTEXT_DOMAIN),
      "value" => Array(__("Dark Background", GETTEXT_DOMAIN) => 'dark_bg')
    ),
    
   )
));

?>