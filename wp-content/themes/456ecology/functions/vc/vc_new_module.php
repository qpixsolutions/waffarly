<?php

function vc_new_module_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
		'image' => '',
		'caption' => '',
		'caption_color' => '',
		'title' => '',
		'title_fs' => '',
		'desc' => '',
		'desc_ls' => '',
		'desc_fs' => '',
		'margin' => '',
		'url' => '',
      
   ), $atts ) );
   
   $image_cropped = wp_get_attachment_image_src( $image, 'new-module' );
   
   ob_start();?>
   
	<a href="<?php echo $url;?>" class="new-lpd-module" <?php if($caption_color){?> style="color:<?php echo $caption_color;?>"<?php }?>>
		<img class="img-responsive" src="<?php echo $image_cropped[0];?>">
		<span class="new_module_content<?php if($caption){?> <?php echo $caption;?><?php }?>"><table><tbody><tr><td style="vertical-align:middle">
		
		<?php if($title){?><span class="title"<?php if($title_fs||$margin){?> style="font-size:<?php echo $title_fs;?>px;line-height:<?php if($title_fs=='60'){echo 60;} elseif($title_fs=='48'){echo 50;} elseif($title_fs=='36'){echo 40;}?>px;margin-left:<?php echo $margin;?>px;margin-right:<?php echo $margin;?>px;"<?php }?>><?php echo $title;?></span><?php }?>
		<?php if($desc){?><span class="description<?php if($desc_ls){?> <?php echo $desc_ls;?><?php }?>"<?php if($desc_fs||$margin){?> style="font-size:<?php echo $desc_fs;?>px;margin-left:<?php echo $margin;?>px;margin-right:<?php echo $margin;?>px;"<?php }?>><?php echo $desc;?></span><?php }?>
		
		</td></tr></tbody></table></span>
	</a>
   
   	<?php return ob_get_clean();
   
}
add_shortcode( 'vc_new_module', 'vc_new_module_func' );


vc_map(array(
   "name" => __("New Module", GETTEXT_DOMAIN),
   "base" => "vc_new_module",
   "class" => "",
   "icon" => "icon-wpb-lpd",
   "category" => __('Content', GETTEXT_DOMAIN),
   'admin_enqueue_js' => "",
   'admin_enqueue_css' => array(get_template_directory_uri().'/functions/vc/assets/vc_extend.css'),
   "params" => array(
	    array(
	      "type" => "attach_image",
	      "heading" => __("Image", GETTEXT_DOMAIN),
	      "param_name" => "image",
	      "value" => "",
	      "description" => __("Select image from media library.", GETTEXT_DOMAIN)
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Caption", GETTEXT_DOMAIN),
	      "param_name" => "caption",
	      "value" => array( __("left", GETTEXT_DOMAIN) => "",  __("center", GETTEXT_DOMAIN) => "align-center", __("right", GETTEXT_DOMAIN) => "align-right"),
	      "description" => __("Select caption location.", GETTEXT_DOMAIN)
	    ),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Caption Color", GETTEXT_DOMAIN),
			"param_name" => "caption_color",
			"value" => '#555',
			"description" => __("Choose caption font color.", GETTEXT_DOMAIN)
		),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", GETTEXT_DOMAIN),
			 "param_name" => "title",
			 "value" => '',
			 "description" => __("Title for module.", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Title Font size", GETTEXT_DOMAIN),
	      "param_name" => "title_fs",
	      "value" => array( "60px" => "60", '48px' => "48", '36px' => "36"),
	      "description" => __("Title font size.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Description", GETTEXT_DOMAIN),
			 "param_name" => "desc",
			 "value" => '',
			 "description" => __("Description for module.", GETTEXT_DOMAIN)
		),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Description Letter Spacing", GETTEXT_DOMAIN),
	      "param_name" => "desc_ls",
	      "value" => array( __("none", GETTEXT_DOMAIN) => "", '5px' => "letter-space", '10px' => "letter-space-10"),
	      "description" => __("Description letter spacing.", GETTEXT_DOMAIN)
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Description Font size", GETTEXT_DOMAIN),
	      "param_name" => "desc_fs",
	      "value" => array( "18px" => "18", '16px' => "16", '14px' => "14"),
	      "description" => __("Description font size.", GETTEXT_DOMAIN)
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => __("Margin", GETTEXT_DOMAIN),
	      "param_name" => "margin",
	      "value" => array( "40px" => "40", '20px' => "20"),
	      "description" => __("Choose left right margin for module caption.", GETTEXT_DOMAIN)
	    ),
		array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Url (link)", GETTEXT_DOMAIN),
			 "param_name" => "url",
			 "value" => "#",
			 "description" => __("Module link.", GETTEXT_DOMAIN)
		),
   )
));

?>