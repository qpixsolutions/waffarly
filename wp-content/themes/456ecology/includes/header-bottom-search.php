<?php $navigation_search= ot_get_option('navigation_search');?>

<?php if($navigation_search!="none"):?>
<div class="header-bottom-search">
	<?php if($navigation_search!="none"):?>
    <div class="header-search hidden-xs hidden-sm">
        <a class="search-icon<?php if (is_plugin_active('sitepress-multilingual-cms/sitepress.php')):?><?php if($wpml_switcher):?> sep-line<?php endif; ?><?php endif; ?>" href="#"></a>
        <div class="search-dropdown css-transition-all-3-2">
        <div class="search-dropdown-full-width">
        <div class="container">
			<?php if($navigation_search == "shop_search"){ ?>
				<form role="form" method="get" class="" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
					<input type="hidden" name="post_type" value="product" />
				    <input type="input" class="form-control" id="s" name="s" placeholder="<?php _e( 'Search For Products', GETTEXT_DOMAIN ); ?>">
					<button type="submit" class="hide"></button>
				</form>
			<?php } elseif($navigation_search == "theme_search"){?>
				<form role="form" method="get" class="" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
				    <input type="input" class="form-control" id="s" name="s" placeholder="<?php _e( 'Search Site', GETTEXT_DOMAIN ); ?>">
					<button type="submit" class="hide"></button>
				</form>
			<?php }?>
        </div>
        </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>