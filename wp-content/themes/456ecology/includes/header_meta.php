<?php $header_meta= ot_get_option('header_meta');?>

<?php if(!$header_meta):?>
<div class="header-top">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php get_template_part('includes/left_header_meta' ) ?>
			</div>
			<div class="col-md-6">
				<?php get_template_part('includes/right_header_meta' ) ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>