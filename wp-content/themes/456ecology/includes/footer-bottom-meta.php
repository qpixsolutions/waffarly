<?php
$cc = ot_get_option('cc');

$footer_copyright = ot_get_option('footer_copyright');
$footer_logo = ot_get_option('footer_logo');

?>

<?php if($cc){ ?>
<div class="col-md-6">
<?php } else{ ?>
<div class="col-md-12">
<?php } ?>
	<?php get_template_part('includes/footer-logo' ) ?>
	<?php if ( has_nav_menu( 'footer-menu' )||$footer_copyright) {?>
		<div class="footer-m-copyright<?php if (!$footer_logo) {?> no-footer-logo<?php } ?>">
		<?php get_template_part('includes/copyright' ) ?>
		<?php if ( has_nav_menu( 'footer-menu' ) ) {?>
		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'footer-menu', 'container' => '', 'depth' => 1  ) ); ?>
		<?php } ?>
		</div>
	<?php } ?>
	<div class="clearfix"></div>
</div>