<?php
$disable_sticky = ot_get_option('disable_sticky');
$body_font_family = ot_get_option('body_font_family');
$heading_font_family = ot_get_option('heading_font_family');
$elements_font_family = ot_get_option('elements_font_family');

$animation_main = ot_get_option('animation_main');
$animation_sidebar = ot_get_option('animation_sidebar');
$animation_footer_top = ot_get_option('animation_footer_top');
$animation_footer_meta = ot_get_option('animation_footer_meta');
$animation_footer = ot_get_option('animation_footer');
$animation_footer_bottom = ot_get_option('animation_footer_bottom');
$animation_blog = ot_get_option('animation_blog');
$animation_portfolio = ot_get_option('animation_portfolio');
$animation_team = ot_get_option('animation_team');
$animation_product = ot_get_option('animation_product');
$animation_shop = ot_get_option('animation_shop');

if(!$disable_sticky){
    add_action( 'wp_enqueue_scripts', 'lpd_sticky_menu' );
}

if (is_plugin_active('woocommerce/woocommerce.php')) {
	add_action( 'wp_enqueue_scripts', 'lpd_woocommerce_styles' );
}

if($body_font_family == 'Open+Sans'
|| $body_font_family == 'Titillium+Web'
|| $body_font_family == 'Oxygen'
|| $body_font_family == 'Quicksand'
|| $body_font_family == 'Lato'
|| $body_font_family == 'Raleway'
|| $body_font_family == 'Source+Sans+Pro'
|| $body_font_family == 'Dosis'
|| $body_font_family == 'Exo'
|| $body_font_family == 'Arvo'
|| $body_font_family == 'Vollkorn'
|| $body_font_family == 'Ubuntu'
|| $body_font_family == 'PT+Sans'
|| $body_font_family == 'PT+Serif'
|| $body_font_family == 'Droid+Sans'
|| $body_font_family == 'Droid+Serif'
|| $body_font_family == 'Cabin'
|| $body_font_family == 'Lora'
|| $body_font_family == 'Oswald'
|| $body_font_family == 'Varela+Round'
|| $body_font_family == 'Unna'
|| $body_font_family == 'Rokkitt'
|| $body_font_family == 'Merriweather'
|| $body_font_family == 'Bitter'
|| $body_font_family == 'Kreon'
|| $body_font_family == 'Playfair+Display'
|| $body_font_family == 'Roboto+Slab'
|| $body_font_family == 'Bree+Serif'
|| $body_font_family == 'Libre+Baskerville'
|| $body_font_family == 'Cantata+One'
|| $body_font_family == 'Alegreya'
|| $body_font_family == 'Noto+Serif'
|| $body_font_family == 'EB+Garamond'
|| $body_font_family == 'Noticia+Text'
|| $body_font_family == 'Old+Standard+TT'
|| $body_font_family == 'Crimson+Text'
|| $body_font_family == 'Josefin+Sans'
|| $body_font_family == 'Ubuntu'

|| $heading_font_family == 'Open+Sans'
|| $heading_font_family == 'Titillium+Web'
|| $heading_font_family == 'Oxygen'
|| $heading_font_family == 'Quicksand'
|| $heading_font_family == 'Lato'
|| $heading_font_family == 'Raleway'
|| $heading_font_family == 'Source+Sans+Pro'
|| $heading_font_family == 'Dosis'
|| $heading_font_family == 'Exo'
|| $heading_font_family == 'Arvo'
|| $heading_font_family == 'Vollkorn'
|| $heading_font_family == 'Ubuntu'
|| $heading_font_family == 'PT+Sans'
|| $heading_font_family == 'PT+Serif'
|| $heading_font_family == 'Droid+Sans'
|| $heading_font_family == 'Droid+Serif'
|| $heading_font_family == 'Cabin'
|| $heading_font_family == 'Lora'
|| $heading_font_family == 'Oswald'
|| $heading_font_family == 'Varela+Round'
|| $heading_font_family == 'Unna'
|| $heading_font_family == 'Rokkitt'
|| $heading_font_family == 'Merriweather'
|| $heading_font_family == 'Bitter'
|| $heading_font_family == 'Kreon'
|| $heading_font_family == 'Playfair+Display'
|| $heading_font_family == 'Roboto+Slab'
|| $heading_font_family == 'Bree+Serif'
|| $heading_font_family == 'Libre+Baskerville'
|| $heading_font_family == 'Cantata+One'
|| $heading_font_family == 'Alegreya'
|| $heading_font_family == 'Noto+Serif'
|| $heading_font_family == 'EB+Garamond'
|| $heading_font_family == 'Noticia+Text'
|| $heading_font_family == 'Old+Standard+TT'
|| $heading_font_family == 'Crimson+Text'
|| $heading_font_family == 'Josefin+Sans'
|| $heading_font_family == 'Ubuntu'

|| $elements_font_family == 'Open+Sans'
|| $elements_font_family == 'Titillium+Web'
|| $elements_font_family == 'Oxygen'
|| $elements_font_family == 'Quicksand'
|| $elements_font_family == 'Lato'
|| $elements_font_family == 'Raleway'
|| $elements_font_family == 'Source+Sans+Pro'
|| $elements_font_family == 'Dosis'
|| $elements_font_family == 'Exo'
|| $elements_font_family == 'Arvo'
|| $elements_font_family == 'Vollkorn'
|| $elements_font_family == 'Ubuntu'
|| $elements_font_family == 'PT+Sans'
|| $elements_font_family == 'PT+Serif'
|| $elements_font_family == 'Droid+Sans'
|| $elements_font_family == 'Droid+Serif'
|| $elements_font_family == 'Cabin'
|| $elements_font_family == 'Lora'
|| $elements_font_family == 'Oswald'
|| $elements_font_family == 'Varela+Round'
|| $elements_font_family == 'Unna'
|| $elements_font_family == 'Rokkitt'
|| $elements_font_family == 'Merriweather'
|| $elements_font_family == 'Bitter'
|| $elements_font_family == 'Kreon'
|| $elements_font_family == 'Playfair+Display'
|| $elements_font_family == 'Roboto+Slab'
|| $elements_font_family == 'Bree+Serif'
|| $elements_font_family == 'Libre+Baskerville'
|| $elements_font_family == 'Cantata+One'
|| $elements_font_family == 'Alegreya'
|| $elements_font_family == 'Noto+Serif'
|| $elements_font_family == 'EB+Garamond'
|| $elements_font_family == 'Noticia+Text'
|| $elements_font_family == 'Old+Standard+TT'
|| $elements_font_family == 'Crimson+Text'
|| $elements_font_family == 'Josefin+Sans'
|| $elements_font_family == 'Ubuntu'
){
	add_action('wp_enqueue_scripts', 'lpd_ggl_web_font');
}else{
	add_action('wp_enqueue_scripts', 'lpd_ggl_web_font_reset');
}

if($animation_main
||$animation_sidebar
||$animation_footer_top
||$animation_footer_meta
||$animation_footer
||$animation_footer_bottom
||$animation_blog
||$animation_portfolio
||$animation_team
||$animation_product){
    add_action( 'wp_enqueue_scripts', 'cre_animate' );	
}

if($animation_main){
    add_action( 'wp_enqueue_scripts', 'ca_custom' );
}
if($animation_sidebar){
    add_action( 'wp_enqueue_scripts', 'ca_custom_sidebar' );
}
if($animation_footer_top){
    add_action( 'wp_enqueue_scripts', 'ca_custom_footer_top' );
}
if($animation_footer_meta){
    add_action( 'wp_enqueue_scripts', 'ca_custom_footer_meta' );
}
if($animation_footer){
    add_action( 'wp_enqueue_scripts', 'ca_custom_footer' );
}
if($animation_footer_bottom){
    add_action( 'wp_enqueue_scripts', 'ca_custom_footer_bottom' );
}
if($animation_blog){
    add_action( 'wp_enqueue_scripts', 'ca_custom_blog' );
}
if($animation_portfolio){
    add_action( 'wp_enqueue_scripts', 'ca_custom_portfolio' );
}
if($animation_team){
    add_action( 'wp_enqueue_scripts', 'ca_custom_team' );
}
if($animation_product){
    add_action( 'wp_enqueue_scripts', 'ca_custom_product' );
}
if($animation_shop){
    add_action( 'wp_enqueue_scripts', 'ca_custom_shop' );
}

?>
