<div id="title-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<h2><?php echo lpd_title();?></h2>
			<div class="deco-sep-line-100"></div>
			<?php if (is_plugin_active('woocommerce/woocommerce.php')) {?>
				<?php if(is_shop()){?>
					<div class="lpd_breadcrumb"><?php _e('You Are Here', GETTEXT_DOMAIN); ?>: <?php echo woocommerce_page_breadcrumb();?></div>
				<?php } else if (is_singular('product') ) {?>
					<div class="lpd_breadcrumb"><?php _e('You Are Here', GETTEXT_DOMAIN); ?>: <?php echo woocommerce_page_breadcrumb();?></div>
				<?php } else if (is_tax('product_cat')||is_tax('product_tag') ) {?>
					<div class="lpd_breadcrumb"><?php _e('You Are Here', GETTEXT_DOMAIN); ?>: <?php echo lpd_breadcrumb()?></div>
				<?php } else{?>
					<div class="lpd_breadcrumb"><?php _e('You Are Here', GETTEXT_DOMAIN); ?>: <?php echo lpd_breadcrumb()?></div>
				<?php }?>
			<?php }else{?>
				<div class="lpd_breadcrumb"><?php _e('You Are Here', GETTEXT_DOMAIN); ?>: <?php echo lpd_breadcrumb()?></div>
			<?php }?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12"><div class="sep-border"></div></div>
		</div>
	</div>
</div>