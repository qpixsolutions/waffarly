<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */?>
            </div>
			
			<?php if(is_shop()){ ?>
				<?php if ( is_active_sidebar(4) ){?>
					<div class="col-md-3">
					    <div class="sidebar">
					    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Shop Sidebar') ) ?>
					    </div>
					</div>
				<?php } ?>
			<?php } ?>
			
		</div>
	</div>
</div>