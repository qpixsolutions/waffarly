<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post, $woocommerce_loop;

$product_options_related_dark = get_post_meta($post->ID, 'product_options_related_dark', true);
$product_options_related_img = get_post_meta($post->ID, 'product_options_related_img', true);
$product_options_related_img = wp_get_attachment_image_src( $product_options_related_img, 'full' );

$related = $product->get_related( $posts_per_page );

if ( sizeof( $related ) == 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'				=> 'product',
	'ignore_sticky_posts'	=> 1,
	'no_found_rows' 		=> 1,
	'posts_per_page' 		=> $posts_per_page,
	'orderby' 				=> $orderby,
	'post__in' 				=> $related,
	'post__not_in'			=> array( $product->id )
) );

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

	<?php if ($product_options_related_img) : ?>

	<div style="background-image: url(<?php echo $product_options_related_img[0]; ?>);" class="parallax-object parallax-product parallax-full<?php if($product_options_related_dark){echo ' dark-plx-bg';}else{echo ' light-plx-bg';} if($product->get_upsells()){echo " up-sell-item";}?>" data-stellar-background-ratio="0.3">
	
		<div class="container">
		
	<?php endif; ?>

		<div class="lpd-related lpd-products">
	
			<h2 <?php if (!$product_options_related_img) : ?>class="lpd-heading-title"<?php endif; ?>><span><?php _e( 'Related Products', 'woocommerce' ); ?></span></h2>
	
			<?php woocommerce_product_loop_start(); ?>
	
				<?php while ( $products->have_posts() ) : $products->the_post(); ?>
	
					<?php wc_get_template_part( 'content', 'product' ); ?>
	
				<?php endwhile; // end of the loop. ?>
	
			<?php woocommerce_product_loop_end(); ?>
	
		</div>
		
			
	<?php if ($product_options_related_img) : ?>
				
		</div>
	
	</div>
	
	<?php endif; ?> 

<?php endif;

wp_reset_postdata();
