<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>


<div class="panel-group lpd-prodcut-accordion" id="accordion">
	<?php $i = 1; foreach ( $tabs as $key => $tab ) : ?>
	<?php $count = $i++; ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#tab-<?php echo $key ?>" class="<?php if($count>1){echo ' collapsed';}?>">
					<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?>
				</a>
			</span>
	    </div>
		<div id="tab-<?php echo $key ?>" class="panel-collapse collapse<?php if($count==1){echo ' in';}?>">
			<div class="panel-body">
				<?php call_user_func( $tab['callback'], $key, $tab ) ?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>



<?php endif; ?>