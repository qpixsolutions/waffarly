<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post, $woocommerce_loop;

$product_options_up_sells_dark = get_post_meta($post->ID, 'product_options_up_sells_dark', true);
$product_options_up_sells_img = get_post_meta($post->ID, 'product_options_up_sells_img', true);
$product_options_up_sells_img = wp_get_attachment_image_src( $product_options_up_sells_img, 'full' );

$upsells = $product->get_upsells();

if ( sizeof( $upsells ) == 0 ) return;

$meta_query = WC()->query->get_meta_query();

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $posts_per_page,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array( $product->id ),
	'meta_query'          => $meta_query
);

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

	<?php if ($product_options_up_sells_img) : ?>

	<div style="background-image: url(<?php echo $product_options_up_sells_img[0]; ?>);" class="parallax-object parallax-product parallax-full<?php if($product_options_up_sells_dark){echo ' dark-plx-bg';}else{echo ' light-plx-bg';} ?>" data-stellar-background-ratio="0.3">
	
		<div class="container">
		
	<?php endif; ?>
		
			<div class="lpd-upsells lpd-products">
		
				<h2 <?php if (!$product_options_up_sells_img) : ?>class="lpd-heading-title"<?php endif; ?>><span><?php _e( 'You may also like&hellip;', 'woocommerce' ) ?></span></h2>
		
				<?php woocommerce_product_loop_start(); ?>
		
					<?php while ( $products->have_posts() ) : $products->the_post(); ?>
		
						<?php wc_get_template_part( 'content', 'product' ); ?>
		
					<?php endwhile; // end of the loop. ?>
		
				<?php woocommerce_product_loop_end(); ?>
		
			
			</div>
			
	<?php if ($product_options_up_sells_img) : ?>
				
		</div>
	
	</div>
	
	<?php endif; ?> 

<?php endif;

wp_reset_postdata();
