<?php require_once(ABSPATH .'/wp-admin/includes/plugin.php');?>

<?php get_template_part('includes/layouts' ); ?>
<?php get_template_part('includes/plug' );?>
<?php $navigation_color = ot_get_option('navigation_color');?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    
	<?php get_template_part('includes/title' ) ?>
    
	<?php get_template_part('includes/seo' ) ?>

	<?php get_template_part('includes/meta-viewport' ) ?>
    
    <meta name="author" content="lidplussdesign" />

    <?php get_template_part('includes/favicon' ) ?>

    <?php wp_head(); ?>
    
</head>
<body <?php body_class(); ?>>

<div id="body-wrap">
	<div id="header">
		<?php get_template_part('includes/header_meta' ) ?>
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-md-12">					
						<?php get_template_part('includes/header-middle' ) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="header-bottom-wrap"<?php if ($navigation_color) {?>  style="background-color:<?php echo $navigation_color;?>"<?php } ?>>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
					        <?php get_template_part('includes/header-bottom-search' ) ?>
							<?php get_template_part('includes/menu' ) ?>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
