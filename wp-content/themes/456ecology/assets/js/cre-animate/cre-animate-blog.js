///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_blog();
});

function ca_custom_blog() {	
	
	jQuery( ".type-post,.single-post .single-post,.single-post .comment-respond" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}