///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_product();
});

function ca_custom_product() {	
	
	jQuery( ".type-product .product-image,.type-product .product-content,.lpd-prodcut-description,.single-product .lpd-related,.single-product .lpd-upsells,.single-product .parallax-product" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}