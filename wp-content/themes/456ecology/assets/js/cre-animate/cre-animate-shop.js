///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_shop();
});

function ca_custom_shop() {	
	
	jQuery( ".post-type-archive-product .type-product,.tax-product_cat .type-product,.tax-product_tag .type-product" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}