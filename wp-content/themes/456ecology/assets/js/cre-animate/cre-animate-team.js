///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_team();
});

function ca_custom_team() {	
	
	jQuery( ".single-team .single-post" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}