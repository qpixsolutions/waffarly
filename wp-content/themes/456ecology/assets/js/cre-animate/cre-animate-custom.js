///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom();
});

function ca_custom() {

	jQuery( ".vc_row-fluid.lpd-animation" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}