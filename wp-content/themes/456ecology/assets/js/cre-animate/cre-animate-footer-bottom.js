///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_footer_bottom();
});

function ca_custom_footer_bottom() {	
	
	jQuery( ".footer-bottom .row" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','98%')
	.attr('data-easing','ease');
	
}