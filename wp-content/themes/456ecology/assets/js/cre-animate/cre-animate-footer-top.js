///*global jQuery:false */
jQuery(document).ready(function() {
	"use strict";	
    ca_custom_footer_top();
});

function ca_custom_footer_top() {	
	
	jQuery( ".footer-top .widget" )
	.addClass('cre-animate')
	.css('opacity', '0')
	.attr('data-animation','fade-in')
	.attr('data-speed','500')
	.attr('data-delay','0')
	.attr('data-offset','80%')
	.attr('data-easing','ease');
	
}