<?php

/* URI shortcuts
================================================== */
define( 'THEME_ASSETS', get_template_directory_uri() . '/assets/', true );
define( 'TEMPLATEPATH', get_template_directory_uri(), true );
define( 'GETTEXT_DOMAIN', '456ecology' );

require_once dirname( __FILE__ ) . '/functions/reset.php';

require_once dirname( __FILE__ ) . '/functions/class-tgm-plugin-activation.php';
require_once dirname( __FILE__ ) . '/functions/class-tgm-plugin-register.php';


/* Register and load JS, CSS
================================================== */
function lpd_enqueue_scripts() {

    wp_register_style('woocommerce', THEME_ASSETS . 'css/woocommerce.css');
    
	wp_register_style('non-responsive-css', THEME_ASSETS . 'css/non-responsive.css');
    wp_register_script('non-responsive-js', THEME_ASSETS.'js/non-responsive.js', false, false, true);
    
	// register scripts;
    wp_register_script('bootstrap', THEME_ASSETS.'js/bootstrap.js', false, false, true);
    wp_register_script('custom', THEME_ASSETS.'js/custom.functions.js', false, false, true);
    wp_register_script('sticky-menu', THEME_ASSETS.'js/sticky_menu.js', false, false, true);
    wp_register_script('menu3d', THEME_ASSETS.'js/menu3d.js', false, false, true);
    
    wp_register_script('stellar', THEME_ASSETS.'js/jquery.stellar.min.js', false, false, true);
    
    // cube
    wp_register_script('cubeportfolio', THEME_ASSETS.'cbp-plugin/cubeportfolio/js/jquery.cubeportfolio.min.js', false, false, true);
    // END cube

    // multi slider
    wp_register_script('custom_modernizr', THEME_ASSETS.'js/modernizr.custom.63321.js', false, false, true);
    wp_register_script('multi_slider', THEME_ASSETS.'js/jquery.catslider.js', false, false, true);
    // end multi slider
    
    // pie chart 
    wp_register_script('easypiechart', THEME_ASSETS.'js/easypiechart.js', false, false, true);
    // pie chart end
    
    // ca custom
    wp_register_script('cre-animate', THEME_ASSETS.'js/jquery.cre-animate.js', false, false, true);
    
    wp_register_script('ca_custom', THEME_ASSETS.'js/cre-animate/cre-animate-custom.js', false, false, true);
    wp_register_script('ca_custom_blog', THEME_ASSETS.'js/cre-animate/cre-animate-blog.js', false, false, true);
    wp_register_script('ca_custom_footer_bottom', THEME_ASSETS.'js/cre-animate/cre-animate-footer-bottom.js', false, false, true);
    wp_register_script('ca_custom_footer_meta', THEME_ASSETS.'js/cre-animate/cre-animate-footer-meta.js', false, false, true);
    wp_register_script('ca_custom_footer_top', THEME_ASSETS.'js/cre-animate/cre-animate-footer-top.js', false, false, true);
    wp_register_script('ca_custom_footer', THEME_ASSETS.'js/cre-animate/cre-animate-footer.js', false, false, true);
    wp_register_script('ca_custom_portfolio', THEME_ASSETS.'js/cre-animate/cre-animate-portfolio.js', false, false, true);
    wp_register_script('ca_custom_product', THEME_ASSETS.'js/cre-animate/cre-animate-product.js', false, false, true);
    wp_register_script('ca_custom_sidebar', THEME_ASSETS.'js/cre-animate/cre-animate-sidebar.js', false, false, true);
    wp_register_script('ca_custom_team', THEME_ASSETS.'js/cre-animate/cre-animate-team.js', false, false, true);
    wp_register_script('ca_custom_shop', THEME_ASSETS.'js/cre-animate/cre-animate-shop.js', false, false, true);
    // ca custom
	
	// enqueue scripts
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap');
	wp_enqueue_script('custom');
	wp_enqueue_script('menu3d');
	wp_enqueue_script('stellar');
	wp_enqueue_script('waypoints');
	
	wp_enqueue_script('cubeportfolio');
 
    if ( is_singular() ) wp_enqueue_script( "comment-reply" );   

}
add_action('wp_enqueue_scripts', 'lpd_enqueue_scripts');


function vc_admin() {

    // visual composer front admin
    wp_register_style('vc-extend-front', get_template_directory_uri() . '/functions/vc/assets/vc_extend_front.css');
    // visual composer front admin end
    
	wp_enqueue_style('vc-extend-front');
}
add_action( 'admin_enqueue_scripts', 'vc_admin' );

function lpd_theme_style() {
	wp_enqueue_style( 'lpd-style', get_bloginfo( 'stylesheet_url' ), array(), '1.0' );
}
add_action( 'wp_enqueue_scripts', 'lpd_theme_style' );

function lpd_ie_selectivizr() {
	global $is_IE;
	if ( $is_IE ) {
		echo '<!--[if IE 8]>';
		echo '<script src="'.THEME_ASSETS.'js/selectivizr.js"></script>';
		echo '<![endif]-->';
	}
}
add_action( 'wp_head', 'lpd_ie_selectivizr' );

function lpd_ie_html5() {
	global $is_IE;
	if ( $is_IE ) {
		echo '<!--[if lt IE 9]>';
		echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
		echo '<![endif]-->';
	}
}
add_action( 'wp_head', 'lpd_ie_html5' );

function lpd_fix_ie_css_limit() {
	global $is_IE;
	if ( $is_IE ) {
		echo '<!--[if IE]>';
		echo '<script src="'.THEME_ASSETS.'js/fix-ie-css-limit-standalone.min.js"></script>';
		echo '<![endif]-->';
	}
}
add_action( 'wp_footer', 'lpd_fix_ie_css_limit',99 );

function easypiechart() {
	wp_enqueue_script('easypiechart');	
}
function lpd_woocommerce_styles() {
	wp_enqueue_style('woocommerce');
}
function multi_slider() {
	wp_enqueue_script('custom_modernizr');
	wp_enqueue_script('multi_slider');
}
function lpd_sticky_menu() {
	wp_enqueue_script('sticky-menu');
}
function lpd_fixed_1170() {
	wp_enqueue_script('non-responsive-js');
	wp_enqueue_style('non-responsive-css');
}
function lpd_add_admin_scripts( $hook ) {

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {    
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_script('custom-js', get_template_directory_uri().'/functions/metabox/js/custom-js.js');
	wp_enqueue_style('jquery-ui-custom', get_template_directory_uri().'/functions/metabox/css/jquery-ui-custom.css');
    }
}
add_action( 'admin_enqueue_scripts', 'lpd_add_admin_scripts', 10, 1 );

function cre_animate() {
	wp_enqueue_script('cre-animate');
}
function ca_custom() {
	wp_enqueue_script('ca_custom');
}
function ca_custom_blog() {
	wp_enqueue_script('ca_custom_blog');
}
function ca_custom_footer_bottom() {
	wp_enqueue_script('ca_custom_footer_bottom');
}
function ca_custom_footer_meta() {
	wp_enqueue_script('ca_custom_footer_meta');
}
function ca_custom_footer_top() {
	wp_enqueue_script('ca_custom_footer_top');
}
function ca_custom_footer() {
	wp_enqueue_script('ca_custom_footer');
}
function ca_custom_portfolio() {
	wp_enqueue_script('ca_custom_portfolio');
}
function ca_custom_product() {
	wp_enqueue_script('ca_custom_product');
}
function ca_custom_sidebar() {
	wp_enqueue_script('ca_custom_sidebar');
}
function ca_custom_team() {
	wp_enqueue_script('ca_custom_team');
}
function ca_custom_shop() {
	wp_enqueue_script('ca_custom_shop');
}

require_once dirname( __FILE__ ) . '/functions/sidebar.php';
require_once dirname( __FILE__ ) . '/functions/Functions.php';


require_once (TEMPLATEPATH. '/functions/theme_walker.php');
require_once (TEMPLATEPATH. '/functions/menu3dmega-options.php');
require_once (TEMPLATEPATH. '/functions/theme_video.php');
require_once (TEMPLATEPATH. '/functions/theme_comments.php');
require_once (TEMPLATEPATH. '/functions/theme_breadcrumb.php');
require_once (TEMPLATEPATH. '/functions/lpd_title.php');
require_once (TEMPLATEPATH. '/functions/shortcodes.php');
include_once (TEMPLATEPATH. '/functions/woocommerce.php');
include_once (TEMPLATEPATH. '/functions/ggl_web_fonts.php');

if (is_plugin_active('js_composer/js_composer.php')) {
	require_once (TEMPLATEPATH. '/functions/vc/vc_featured_modules.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_module.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_new_module.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_multi_module.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_post_widget.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_cube_widget.php');

	require_once (TEMPLATEPATH. '/functions/vc/vc_multi_slider.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_counter.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_meta_block.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_meta_container.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_iconitem.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_blockquote.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_new_button.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_callout.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_callout2.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_new_title.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_mega_header.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_testimonial.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_add_shortcode.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_woocommerce.php');
	require_once (TEMPLATEPATH. '/functions/vc/vc_lpd_woocommerce.php');
}

/* functions custom styles
================================================== */
require_once (TEMPLATEPATH. '/functions/fonts.php');
require_once (TEMPLATEPATH. '/functions/background.php');
require_once (TEMPLATEPATH. '/functions/color.php');
if (is_plugin_active('woocommerce/woocommerce.php')) {require_once (TEMPLATEPATH. '/functions/shop-styles.php');}

/* post type
================================================== */
include_once (TEMPLATEPATH. '/functions/post-type/portfolio.php');
include_once (TEMPLATEPATH. '/functions/post-type/team.php');

/* metabox
================================================== */
include_once (TEMPLATEPATH. '/functions/metabox/functions/add_meta_box-sidebar.php');
include_once (TEMPLATEPATH. '/functions/metabox/functions/add_meta_box-portfolio-options.php');
include_once (TEMPLATEPATH. '/functions/metabox/functions/add_meta_box-team-options.php');
include_once (TEMPLATEPATH. '/functions/metabox/functions/add_meta_box-product-options.php');

include_once (TEMPLATEPATH. '/functions/metabox-post-format.php');
include_once (TEMPLATEPATH. '/functions/metabox-portfolio-format.php');
require_once (TEMPLATEPATH. '/functions/gallery-metabox/gallery.php');


?>